library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

package readout_constants is

 constant NSLAVES           : positive := 200;
 constant IPBUS_MAXREGS     : positive := 199;
 
 constant IPBADDR_VERSION   : natural :=  0;
 constant IPBADDR_STATUS    : natural :=  1;
 constant IPBADDR_RESET     : natural :=  2;
 constant IPBADDR_BUSY      : natural :=  3;
 constant IPBADDR_SCFIFO_R  : natural :=  4;
 constant IPBADDR_SCFIFO_W  : natural :=  5;
 constant IPBADDR_AOFIFO    : natural :=  6;
 constant IPBADDR_AOMONI    : natural :=  7;
 constant IPBADDR_DUMCTR    : natural :=  8;
 constant IPBADDR_DELAY     : natural :=  9;

 constant AO_ADDR_TAPW      : natural := 10;
 constant AO_ADDR_TAPR      : natural := 50;
 constant AO_ADDR_WDCTR     : natural := 91;
 constant AO_ADDR_DELAY     : natural := 92;

 constant NPINS             : natural := 37;
 constant IPBUS_WIDTH       : natural := 32;
 constant FASTFIFO_WIDTH    : natural := 62; -- bojan 44->62, modified also in fifo_Fast IP
 constant DIFF_WIDTH        : natural := 30; -- 12->30
 constant TRIG_ID_SIZE      : natural := 18;    
 constant FINAL_WORD_LENGTH : natural := 62;
 
 

 constant IPBADDR_SCCONTROL                     : natural := 155; 
 ---- SC LARGE WORDS T0/FROM FIFO
 constant IPBADDR_WWORD                         : natural := 183;
 constant IPBADDR_RWORD                         : natural := 184;
 ---- SC SMALL WORDS
 constant IPBADDR_SC_SMALL_1_W                  : natural := 114;
 constant IPBADDR_LVDS_BRIDGE_CMF_IB_IV         : natural := 115; 
 constant IPBADDR_SWCNTL_IBUFMON                : natural := 116;
 constant IPBADDR_PULSE_MON_DELAY               : natural := 117; 
 constant IPBADDR_SC_SMALL_1_R                  : natural := 118;
 constant IPBADDR_LVDS_BRIDGE_CMF_IB_IV_R       : natural := 119; 
 constant IPBADDR_SWCNTL_IBUFMON_R              : natural := 120;
 constant IPBADDR_PULSE_MON_DELAY_R             : natural := 121; 
 constant IPBADDR_LWORDS                        : natural := 122;
 constant IPBADDR_SPLIT_REG                     : natural := 123;

-- -- ipbus 128 bit fifo ipbus regs
-- constant IPBADDR_SCFIFO_R_SET_VCASN      : natural := 159;
-- constant IPBADDR_SCFIFO_R_SET_VCLIP      : natural := 160;
-- constant IPBADDR_SCFIFO_R_SET_VPLSE_HIGH : natural := 161;
-- constant IPBADDR_SCFIFO_R_SET_VPLSE_LOW  : natural := 162;
-- constant IPBADDR_SCFIFO_R_SET_VRESET_P   : natural := 163;
-- constant IPBADDR_SCFIFO_R_SET_VRESET_D   : natural := 164;
-- constant IPBADDR_SCFIFO_R_SET_ICASN      : natural := 165;
-- constant IPBADDR_SCFIFO_R_SET_IRESET     : natural := 166;
-- constant IPBADDR_SCFIFO_R_SET_ITHR       : natural := 167;
-- constant IPBADDR_SCFIFO_R_SET_IBIAS      : natural := 168;
-- constant IPBADDR_SCFIFO_R_SET_IDB        : natural := 169;
 
-- -- ipbus 256 bit fifo ipbus regs
-- constant IPBADDR_SCFIFO_R_MASK_FULLCOL   : natural := 170;
 
-- -- ipbus 512 bit fifo ipbus regs
-- constant IPBADDR_SCFIFO_R_MASK_COL_1     : natural := 171;
-- constant IPBADDR_SCFIFO_R_MASK_COL_2     : natural := 172;
-- constant IPBADDR_SCFIFO_R_MASK_HOR_1     : natural := 173;
-- constant IPBADDR_SCFIFO_R_MASK_HOR_2     : natural := 174;
-- constant IPBADDR_SCFIFO_R_MASK_DIAG_1    : natural := 175;
-- constant IPBADDR_SCFIFO_R_MASK_DIAG_2    : natural := 176;
-- constant IPBADDR_SCFIFO_R_PULSE_COL_1    : natural := 177;
-- constant IPBADDR_SCFIFO_R_PULSE_COL_2    : natural := 178;
-- constant IPBADDR_SCFIFO_R_PULSE_HOR_1    : natural := 179;
-- constant IPBADDR_SCFIFO_R_PULSE_HOR_2    : natural := 180;
-- constant IPBADDR_SCFIFO_W_SET_VCASN      : natural := 181;
 
-- constant IPBADDR_E_LOAD                  : natural := 182;
-- constant IPBADDR_TEST                    : natural := 199;

------New constants added by Leyre
-- constant IPBADDR_SC_SMALL_1_W            : natural := 114;
-- constant IPBADDR_LVDS_BRIDGE_CMF_IB_IV   : natural := 115; 
-- constant IPBADDR_SWCNTL_IBUFMON          : natural := 116;
-- constant IPBADDR_PULSE_MON_DELAY         : natural := 117; 
-- constant IPBADDR_SC_SMALL_1_R            : natural := 118;
-- constant IPBADDR_LVDS_BRIDGE_CMF_IB_IV_R : natural := 119; 
-- constant IPBADDR_SWCNTL_IBUFMON_R        : natural := 120;
-- constant IPBADDR_PULSE_MON_DELAY_R       : natural := 121; 
 

 type MatrixW is array (0 to NPINS-1) of std_logic_vector(23 downto 0);
end;
