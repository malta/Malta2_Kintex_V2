----------------------------------------------------------------------------------
-- Company: 	IFIC - UV
-- Engineer: 	Fernando Carrio
-- Contact:		fernando.carrio@cern.ch
-- Create Date:    16:59:34 11/24/2017 
-- Design Name: 
-- Module Name:    IDELAY_CFG - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.all;

entity IDELAY_CFG is
    generic ( 
			k_delay 			: std_logic_vector(4 downto 0):= "00010");
	 port (
			i_clk200			: in std_logic;
			i_reset			: in std_logic;
			i_enable			: in std_logic;
			i_data			: in std_logic;
			i_count			: in std_logic_vector(4 downto 0);
			o_data_dly		: out std_logic;
			o_countRBK		: out std_logic_vector(4 downto 0));
end IDELAY_CFG;

architecture behavioral of IDELAY_CFG  is

signal m_count		: std_logic_vector(4 downto 0):=k_delay;
signal m_count_d	: std_logic_vector(4 downto 0):=k_delay;
signal m_enable	    : std_logic := '0';
signal m_enable_f	: std_logic := '0';
signal m_enable_d	: std_logic := '0';

attribute ASYNC_REG : string;
attribute ASYNC_REG of m_enable, m_enable_f: signal is "TRUE";

begin

IDELAYE2_inst : IDELAYE2
generic map (
   CINVCTRL_SEL 			   => "FALSE",
   DELAY_SRC 				   => "IDATAIN",
   HIGH_PERFORMANCE_MODE 	   => "TRUE", 
   IDELAY_TYPE 				   => "VAR_LOAD", 
   IDELAY_VALUE 			   => 0,          
   PIPE_SEL 				   => "FALSE",    
   REFCLK_FREQUENCY 		   => 200.0,      
   SIGNAL_PATTERN 			   => "DATA"      
)
port map (
   CNTVALUEOUT 				    => o_countRBK, 	
   DATAOUT 						=> o_data_dly,
   C 							=> i_clk200,  
   CE 							=> '0',--'1'  
   CINVCTRL 					=> '0',       	
   CNTVALUEIN 					=> m_count,
   DATAIN 						=> '0',     
   IDATAIN 						=> i_data, 
   INC 							=> '1',     
   LD 							=> '1',
   LDPIPEEN 					=> '0',
   REGRST 						=> '0'
);


--m_enable <= i_enable;
process(i_clk200)
begin
    if rising_edge(i_clk200) then
        m_enable <= i_enable;
        m_enable_f <= m_enable;
    end if;
end process;

process(i_clk200)
begin
	if rising_edge(i_clk200) then
		if i_reset ='1' then
			m_count 		<= k_delay;
			m_enable_d 	<= '0';
		else
			m_enable_d <= m_enable_f;
			if m_enable_d = '0' and m_enable_f = '1' then
				m_count 	<= i_count;
			else
				m_count 	<= m_count;
			end if;
		end if;
	end if;
end process;
end  behavioral;