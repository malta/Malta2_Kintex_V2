library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity SlowCounter is
 Port ( 
  i_clk   : in std_logic;
  
  i_rst   : in std_logic;
  
  o_count : out std_logic_vector( 3 downto 0)
 );
end SlowCounter;

architecture Main of SlowCounter is
  
 signal m_count : natural range 0 to 15 := 0; 
 signal m_s : std_logic := '0'; 
 signal m_vector : std_logic_vector (3 downto 0) := (others=>'0'); 
 
 attribute ASYNC_REG : string;
 attribute ASYNC_REG of m_vector : signal is "TRUE";

begin

 o_count <= std_logic_vector(to_unsigned(m_count,4));

process (i_rst) is 
 begin  
     if rising_edge(i_rst) then
        m_s <= not m_s;
     end if;    
 end process;

process (i_clk) is 
begin  
 if rising_edge(i_clk) then
      m_vector(0) <= m_s;
     for i in 0 to 2 loop
        m_vector(i+1) <= m_vector(i);
     end loop;
 end if;    
end process;

process (i_clk) is 
begin  
    if rising_edge(i_clk) then 
        if (m_vector(3) xor m_vector(2))='1' then
            m_count <= 0; -- 4
        else
            m_count <= m_count+1;
        end if; 
    end if;    
end process;

end Main;
