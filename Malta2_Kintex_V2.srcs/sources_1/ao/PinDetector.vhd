library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;

--would also need some triggering/ready logic

entity PinDetector is
 Port ( 
  --i_clk        : in  std_logic;
  i_sample     : in  std_logic_vector(7 downto 0); 
  i_sampleRef  : in  std_logic_vector(7 downto 0);
  --i_re         : in std_logic; 
  o_result     : out std_logic
 );
end PinDetector;

architecture Main of PinDetector is

 signal m_result : std_logic := '0';
 signal m_bit    : std_logic := '0';
 signal m_re     : std_logic ;
begin
 --m_re <= i_re;
 --very stupid logic for the moment
-- process(i_clk)
-- begin
--   if rising_edge(i_clk) then
--    case i_sample is
--     when "11110000" => m_bit <= i_sample(1) or i_sample(2);        
--     when "01111000" => m_bit <= i_sample(2) or i_sample(3);  
--     when "00111100" => m_bit <= i_sample(3) or i_sample(4);
--     when "00011110" => m_bit <= i_sample(4) or i_sample(5);    
--     when "00001111" => m_bit <= i_sample(5) or i_sample(6);
--     when "10000111"|"00000111" => m_bit <= i_sample(6) or i_sample(7);
--     when "11000011"|"10000011"|"00000011" => m_bit <= i_sample(6) or i_sample(7);
--     when "11100001"|"11000001"|"10000001"|"00000001" => m_bit <= i_sample(6) or i_sample(7); 
--     when others => m_bit <= '0';
--    end case;
--   end if;
-- end process;
 
-- process(i_clk)
-- begin
--   if i_re = '1' then
--     m_result <= m_bit;
--   end if;
-- end process;

--- VD on 19-03-2016: removing some of the bit snice it should be still be robust when the signals are long

 m_result <= ( i_sample(0) and i_sampleRef(0) ) or 
             ( i_sample(1) and i_sampleRef(1) ) or 
             ( i_sample(2) and i_sampleRef(2) ) or
             ( i_sample(3) and i_sampleRef(3) ) or  
             ( i_sample(4) and i_sampleRef(4) ) or
             ( i_sample(5) and i_sampleRef(5) ) or  
             ( i_sample(6) and i_sampleRef(6) ) or 
             ( i_sample(7) and i_sampleRef(7) ) ; 
 
 
 o_result <= m_result;

end Main;
