----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/02/2018 11:03:19 AM
-- Design Name: 
-- Module Name: counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


-- Binary counter with two selectable input modes:
-- Mode 1 (i_mode_clocked = 1): increment the counter at every clock cycle
-- Mode 2 (i_mode_clocked = 0): increment the counter at every input trigger signal
    -- Mode 2.1: 'standalone' architecture: counter increments on rising edges of the trigger signal
    -- Mode 2.2: 'latched_trigger' architecture: counter increments on rising edge of the CLK if trigger is '1' - separate counters for triggered/clocked mode version 2.3 is preferred
    -- Mode 2.3: 'latched_trigger' architecture: counter increments on rising edge of the CLK if trigger is '1' - same counter

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

entity counter is
    generic ( counter_bits  : natural := 6 );
    Port ( i_clk            : in STD_LOGIC;
           i_trig           : in STD_LOGIC;
           i_mode_clocked   : in std_logic;
           i_reset          : in STD_LOGIC;
           i_hold           : in STD_LOGIC;
           i_maxCounterVal  : in STD_LOGIC_VECTOR(8 downto 0) := (others => '0');
           o_counter        : out STD_LOGIC_VECTOR (counter_bits-1 downto 0);
           o_overflow       : out STD_LOGIC);
           
end counter;

---- architecture 1: external trigger increments the counter only on rising edge
--architecture standalone of counter is
--    signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;
--    signal m_int_trigger    : std_logic := '0';
--begin
--    process (i_clk) is begin
--        if (i_mode_clocked = '1') then
--            m_int_trigger <= i_clk;
--        else
--            m_int_trigger <= i_trig;
--        end if;
        
--    end process;
    
--    process (i_clk, m_int_trigger) is begin
--        if i_reset = '1' then      
--            -- reset counters
--            m_counter <= 0;
--        else    
--            if i_hold = '0' then
--                if (rising_edge(m_int_trigger)) then
--                    m_counter <= m_counter+1;
--                end if;
--            end if;
--        end if;    
--    end process;
    
--    o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
--end standalone;


---- architecture 2: external trigger latched with clock - counter increments synchronously
--architecture latched_trigger_two_counters of counter is
--    -- using two internal counters, one for counting clock cycles and one for counting triggers
--    signal m_trig_counter   : natural range 0 to 2**counter_bits-1 := 0;
--    signal m_clk_counter    : natural range 0 to 2**counter_bits-1 := 0;
--    signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;

--begin
--    process (i_reset, i_clk) is begin
--        if i_reset = '1' then      
--            -- reset counters
--            m_trig_counter <= 0;
--            m_clk_counter <= 0;
--        else    
--            if i_hold = '0' then
--                -- when i_hold is set to 1 do not change the counter value
--                -- increment selected (triggered or clocked) counter
--                if (i_mode_clocked = '1') then
--                    if (rising_edge(i_clk)) then                        -- counting clock cycles
--                        m_clk_counter <= m_clk_counter + 1;
--                    end if;
--                elsif (i_mode_clocked = '0') then                     -- counting triggers
--                    if (rising_edge(i_clk) and i_trig = '1') then 
--                        m_trig_counter <= m_trig_counter + 1;
--                    end if;
--                end if;
--            end if;
--        end if;
--        -- connect counter to output LEDs
--        if (i_mode_clocked = '1') then                            
----            o_counter <= std_logic_vector (to_unsigned(m_clk_counter, counter_bits));
--            m_counter <= m_clk_counter;
--        else 
----            o_counter <= std_logic_vector (to_unsigned(m_trig_counter, counter_bits));
--            m_counter <= m_trig_counter;
--        end if;
--    end process;
--    o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
--end latched_trigger_two_counters;


---- architecture 3: external trigger latched with clock - counter increments synchronously, equalize both counters
---- this should be used
---- in clocked mode clock is inverted w.r.t. input clock
--architecture latched_trigger of counter is
--    signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;
--    signal m_int_trigger    : std_logic := '0';
--begin
--    process (i_clk) is begin
--        if (i_mode_clocked = '1') then
--            m_int_trigger <= not i_clk;     -- clock is inverted here
--        else
--            m_int_trigger <= i_trig;
--        end if;
        
--    end process;
    
--    process (i_clk, m_int_trigger) is begin
--        if i_reset = '1' then      
--            -- reset counters
--            m_counter <= 0;
--        else    
--            if i_hold = '0' then
--                if (rising_edge(i_clk) and m_int_trigger='1') then
--                    m_counter <= m_counter+1;
--                end if;
--            end if;
--        end if;    
--    end process;
    
--    o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
--end latched_trigger;

------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------

architecture external_clock of counter is
    signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;
begin
  --reset here is really anasynchronous once nia lifetime reset so it should not e a problem ...
  process (i_clk, i_reset) is begin
    if i_reset = '1' then      
      m_counter <= 0;
    else    
      if rising_edge(i_clk) then
        if i_trig = '1' then
          m_counter <= m_counter+1;
        end if;
      end if;
    end if;    
  end process;
    
  o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
end external_clock;

------------------------------------------------------------------------------------------------------------

architecture external of counter is
    signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;
begin
  process (i_trig, i_reset) is begin
    if i_reset = '1' then      
      m_counter <= 0;
    else    
      if rising_edge(i_trig) then
        m_counter <= m_counter+1;
      end if;
    end if;
  end process;
    
  o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
end external;

--------------------------------------------------------------------------------------------------------------

--architecture internal_backup of counter is
--    signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;
--    signal m_int_trigger    : std_logic := '0';
--    signal m_isReset        : std_logic := '0';
    
--begin
    
----    m_counter_reset <= i_reset or i_trig;
    
--    --VD: need to separate the 2 processes
--    process (i_clk, i_reset) is begin
--        if i_reset = '1' then   
--            m_counter <= 0;
--        else    
        
--          if (rising_edge(i_clk)) then
--             m_counter <= m_counter+1;
--          end if;
--        end if;  
--    end process;
    
----  process (i_clk) is begin
----    if (rising_edge(i_clk)) then    
----      if i_reset='1' and m_isReset='0' then
----        m_counter <= 0;
----        m_isReset <='1';
----      else
----        m_counter <= m_counter+1;
----      end if; 
----    end if;  
----  end process;
    
    
--  o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
--end internal_backup;

------------------------------------------------------------------------------------------------------------

architecture internal of counter is
  signal m_counter        : natural range 0 to 2**counter_bits-1 := 0;
  signal m_int_trigger    : std_logic := '0'; 
  signal m_isReset        : std_logic := '0';
  signal m_overflow       : std_logic := '0';
  signal m_maxCounterVal_Int : natural range 0 to 1000 := 327; 
  
begin

--    m_counter_reset <= i_reset or i_trig;
 m_maxCounterVal_Int <= 167; --327; -- - to_integer(unsigned(i_maxCounterVal));

 process (i_clk, i_reset) is begin
   if i_reset = '1' then
     m_overflow <= '0';
     m_counter <= 0;
   else 
     if (rising_edge(i_clk)) then
       if m_counter = m_maxCounterVal_Int then --2**counter_bits-1 then
         m_overflow <= '1';
       else
         m_counter <= m_counter+1;
       end if;
     end if;
   end if;
 end process;

 o_counter <= std_logic_vector (to_unsigned(m_counter, counter_bits));
 o_overflow <= m_overflow;
end internal;

------------------------------------------------------------------------------------------------------------