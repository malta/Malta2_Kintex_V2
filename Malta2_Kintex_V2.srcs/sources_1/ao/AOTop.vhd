-------------------------------------------------
-- Asynchronous Oversampling Top module
-- 
-- Valerio.Dao@cern.ch
-- November 2017
-------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use work.ipbus.all;
use work.readout_constants.all;

entity AOTop is
 Port ( 
  i_clk_500MHz     : in  std_logic;
  i_clk_500MHz_90  : in  std_logic; 
  i_clk_Idel       : in  std_logic; 
  i_clk_IPb        : in  std_logic; 
  i_clk_40MHz      : in  std_logic;
  
  i_ResetFifo      : in  std_logic;
  
  i_ipbw           : in     ipb_wbus_array(NSLAVES-1 downto 0);
  i_ipbr           : inout  ipb_rbus_array(NSLAVES-1 downto 0);
  
  i_PIN_P          : in  std_logic_vector(NPINS-1 downto 0);
  i_PIN_N          : in  std_logic_vector(NPINS-1 downto 0);
  
  i_trigID_reset   : in std_logic;
  i_trigID_hold    : in std_logic;
  i_trigID_mode    : in std_logic; -- 0 ... counting triggers, 1 ... counting clocks
  i_trigID_trig    : in std_logic;
  
  o_test           : out std_logic_vector(NPINS-1 downto 0);--
  o_refBeforeFifo  : out std_logic;
  o_debug1         : out std_logic;
  o_debug2         : out std_logic;
  o_fastSignal     : out std_logic;

  o_fifo1_empty    : out std_logic;
  o_fifo2_empty    : out std_logic;
  o_fifo2_ProgEmpty: out std_logic;
  o_fifo2_rdCount  : out std_logic_vector(15 downto 0);
  o_fifoM_empty    : out std_logic;
  o_fifo1_Full     : out std_logic;
  o_fifo1_HalfFull : out std_logic;
  o_fifo2_Full     : out std_logic;
  o_fifoM_Full     : out std_logic;
  
  o_trigID_out     : out std_logic_vector(TRIG_ID_SIZE-1 downto 0); 
  o_count_dbg      : out std_logic_vector(2 downto 0);
  
  i_Delay_Counter  : in std_logic_vector(5 downto 0);
  i_maxCounterVal  : in std_logic_vector(8 downto 0);
  
  i_RO_disable            : in  std_logic;
  i_RO_disable_HalfColumn : in  std_logic;
  i_RO_disable_HalfRow    : in  std_logic    
 );
end AOTop;

architecture Main of AOTop is

component fifo_Fast is
 port (
  rst           : IN STD_LOGIC;
  wr_clk        : IN STD_LOGIC;
  rd_clk        : IN STD_LOGIC;
  din           : IN STD_LOGIC_VECTOR(FINAL_WORD_LENGTH-1 DOWNTO 0);
  wr_en         : IN STD_LOGIC;
  rd_en         : IN STD_LOGIC;
  dout          : OUT STD_LOGIC_VECTOR(FINAL_WORD_LENGTH-1 DOWNTO 0);
  full          : OUT STD_LOGIC;
  almost_full   : OUT STD_LOGIC;
  prog_full     : OUT STD_LOGIC;
  empty         : OUT STD_LOGIC
 );
 end component fifo_Fast;

 component fifo_IPbus is
 port (
  rst           : IN STD_LOGIC;
  wr_clk        : IN STD_LOGIC;
  rd_clk        : IN STD_LOGIC;
  din           : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  wr_en         : IN STD_LOGIC;
  rd_en         : IN STD_LOGIC;
  dout          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  full          : OUT STD_LOGIC;
  almost_full   : OUT STD_LOGIC;
  prog_full     : OUT STD_LOGIC;
  prog_empty    : OUT STD_LOGIC;
  almost_empty  : OUT STD_LOGIC;
  empty         : OUT STD_LOGIC;
  rd_data_count : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
 );
 end component fifo_IPbus;


 type MatrixS is array (0 to NPINS-1) of std_logic_vector( 7 downto 0);
-- type MatrixW is array (0 to NPINS-1) of std_logic_vector(23 downto 0);
 type MatrixN is array (0 to NPINS-1) of natural range 0 to 31;
 type tap_type is array (0 to NPINS-1) of std_logic_vector(31 downto 0);
 
 type DelayMatrix is array (0 to 255) of std_logic_vector( 39 downto 0); --was 99
 
 signal m_Delay_Int       : integer range 0 to 255 := 0;
 signal m_Delay_Reg       : std_logic_vector(31 downto 0) := X"00000032"; -- set default to 50
 signal m_Delay_Vec       : std_logic_vector(7 downto 0) := (others => '0');
 
-- clocks
 signal m_clk_500MHz      : std_logic;
 signal m_clk_500MHz_90   : std_logic;
 signal m_clk_500MHz_180  : std_logic;
 signal m_clk_500MHz_270  : std_logic;
 signal m_clk_Idel        : std_logic;
 signal m_clk_IPb         : std_logic;
 signal m_clk_40MHz       : std_logic;       
 
--variables for AO
 signal m_outReady    : std_logic_vector(NPINS-1 downto 0) := (others => '0');
 signal m_sigOR       : std_logic_vector(NPINS-1 downto 0) := (others => '0');  
 signal m_outOR_w1    : std_logic_vector(NPINS-1 downto 0) := (others => '0');    
 signal m_outOR_w2    : std_logic_vector(NPINS-1 downto 0) := (others => '0');    
 signal m_outOR_2     : std_logic_vector(NPINS-1 downto 0) := (others => '0');    
 signal m_sample      : MatrixS; --THIS IS THE MAIN RESULT OF THE AO
 signal m_sample_del  : MatrixS;  
 signal m_out         : MatrixW := (others => (others => '0')); --THIS IS THE MONITORING WINDOW SIGNAL
 signal m_data_delay0 : std_logic_vector(NPINS-1 downto 0) := (others => '0');
 signal m_data_delay5 : std_logic_vector(NPINS-1 downto 0) := (others => '0');
 signal m_RefPin      : std_logic_vector( 7 downto 0) := (others => '0');
 signal m_RefEnable   : std_logic := '0';
 

----finetuning bit
-- constant m_tap5    : MatrixN := ( 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2);
----                               0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36
 constant m_tap0    : MatrixN := ( 5, 3, 6, 2, 3, 7, 2, 6, 6, 3, 7, 3, 2, 7, 2, 6, 6, 2, 7, 2, 3, 7, 2, 5, 2, 7, 3, 5, 2, 7, 3, 6, 3, 5, 2, 3, 7);
 constant m_tap5    : MatrixN := ( 3, 1, 4, 0, 1, 5, 0, 4, 4, 1, 5, 1, 0, 5, 0, 4, 4, 0, 5, 0, 1, 5, 0, 3, 0, 5, 1, 3, 0, 5, 1, 4, 1, 3, 0, 1, 5);

--this is to write the word out
 signal m_finalWord  : std_logic_vector(FINAL_WORD_LENGTH-1 downto 0)      := (others => '0');
 signal m_clkCounter : std_logic_vector( 3 downto 0)      := (others => '0');
 signal m_clkCounterL: std_logic_vector( 8 downto 0)      := (others => '0');
 signal m_edgeCount  : std_logic_vector( 2 downto 0)      := (others => '0');
 signal m_maltaWord  : std_logic_vector(NPINS-1 downto 0) := (others => '0');
 signal m_writeFast  : std_logic := '0'; --this will decide when to write the fast fifo

--this is for FIFOS
 --------------------------------------------------------------------------------------------------------------
-- official FIFOs
 signal m_fifo1_wrEn     : std_logic := '0';
 signal m_fifo1_rdEn     : std_logic := '0';
 signal m_fifo1_empty    : std_logic := '1';
 signal m_fifo1_full     : std_logic := '0';
 signal m_fifo1_halffull : std_logic := '0';
 signal m_fifo1_almost   : std_logic := '0';
 signal m_fifo1_out      : std_logic_vector(FINAL_WORD_LENGTH-1 downto 0) := (others => '0');
 signal m_mon_fifo_enable:std_logic  := '0';

 signal m_fifo2_wrEn  : std_logic := '0';
 signal m_fifo2_rdEn  : std_logic := '0';
 signal m_fifo2_progE : std_logic := '1';
 signal m_fifo2_empty : std_logic := '1';
 signal m_fifo2_full  : std_logic := '0';
 signal m_fifo2_almost: std_logic := '0';
 signal m_fifo2_in    : std_logic_vector(31 downto 0) := (others => '0');  
 signal m_fifo2_out   : std_logic_vector(31 downto 0) := (others => '0');
 signal m_fifo2_rdctr : std_logic_vector(15 downto 0) := (others => '0');
 signal m_ToDelay     : std_logic_vector(39 downto 0) := (others => '0');  
 signal m_FromDelay   : std_logic_vector(39 downto 0) := (others => '0');  
 signal m_Delay       : DelayMatrix := (others => (others => '0'));

 signal m_tap_config  : tap_type := (others=>(others=>'0')); 
 signal m_tap_rbk     : tap_type := (others=>(others=>'0')); 
 
  -- bojan slow (40 MHz) counter signals for trigger id
 signal m_trigID_reset    : std_logic := '0';
 signal m_trigID_hold     : std_logic := '0';
 signal m_trigID_mode     : std_logic := '0';
 signal m_trigID_trig     : std_logic := '0';
 signal m_trigID_out      : std_logic_vector(TRIG_ID_SIZE-1 downto 0) := (others => '0');    -- SLOW_COUNTER_SIZE defined in Readout_constants.vhd
 signal m_LA1_reset       : std_logic := '0';
 signal m_FastClock_reset : std_logic := '0';
 signal m_overflow        : std_logic := '0';
 
 -- final signal to pipeline the entire Malta word
 signal m_isColOK  : std_logic := '1';    
 signal m_isRowOK  : std_logic := '1';    
 signal m_tmpWord  : std_logic_vector(FINAL_WORD_LENGTH-1 downto 0)      := (others => '0');
 signal m_tmpWord2 : std_logic_vector(FINAL_WORD_LENGTH-1 downto 0)      := (others => '0');
 
     
begin
 
  m_clk_500MHz     <= i_clk_500MHz;
  m_clk_500MHz_90  <= i_clk_500MHz_90;
  m_clk_500MHz_180 <= not m_clk_500MHz;
  m_clk_500MHz_270 <= not m_clk_500MHz_90;
  
  m_clk_Idel  <= i_clk_Idel;
  m_clk_IPb   <= i_clk_IPb;
  m_clk_40MHz <= i_clk_40MHz;

--------------------------------------------------------------------------------------------------------------   
  IDELAYCTRL_inst : IDELAYCTRL
  port map (
   RDY    => open,
   REFCLK => m_clk_Idel,
   RST    => '0'
  );
  
--------------------------------------------------------------------------------------------------------------
----this is the real AO
 SD_PACK : for I in 0 to NPINS-1 generate   
 --SD_PACK : for I in 0 to 1 generate   

  oversampling: entity work.oversampling_module(diff_in)
  generic map( 
   N_TAP_0 => m_tap0(I), 
   N_TAP_5 => m_tap5(I) 
  )
  port map(
   i_control          => m_tap_config(I),
   o_status           => m_tap_rbk(I),
   i_clock_200MHz     => m_clk_Idel,
   i_clock_500MHz     => m_clk_500MHz, 
   i_clock_500MHz_90  => m_clk_500MHz_90,
   i_clock_500MHz_180 => m_clk_500MHz_180,
   i_clock_500MHz_270 => m_clk_500MHz_270, 
   i_FMC_P            => i_PIN_P(I),  
   i_FMC_N            => i_PIN_N(I),
   o_sigOR            => m_sigOR(I),
   o_sampled_bits     => m_sample(I),
   o_delayed0         => m_data_delay0(I),
   o_delayed5         => m_data_delay5(I)
  );
    
  visual: entity work.Latch2Bits(Main_2w)
  port map(  
   i_clk      => m_clk_500MHz, 
   i_sigO     => m_sigOR(0),  
   i_sigV     => m_sample(I),  
   o_outReady => m_outReady(I), 
   o_out      => m_out(I)    
  );
 
    
  -- this could be done better (faster) ... but currently for debugging since if you need it USE m_sigOR!!! 
  m_outOR_w1(I) <= m_out(I)(0) or m_out(I)(1) or m_out(I)(2)  or m_out(I)(3)  or m_out(I)(4)  or m_out(I)(5)  or m_out(I)(6)  or m_out(I)(7); 
  m_outOR_w2(I) <= m_out(I)(8) or m_out(I)(9) or m_out(I)(10) or m_out(I)(11) or m_out(I)(12) or m_out(I)(13) or m_out(I)(14) or m_out(I)(15); 
  m_outOR_2(I)  <= m_outOR_w1(I) or m_outOR_w2(I);

  tap_config: entity work.ipbus_register
  port map(
   c_address  => std_logic_vector(to_unsigned(AO_ADDR_TAPW+i,32)),
   i_clk      => i_clk_ipb,
   i_reset    => i_resetFifo,
   i_ipbw     => i_ipbw(AO_ADDR_TAPW+i),
   o_ipbr     => i_ipbr(AO_ADDR_TAPW+i),
   c_readonly => '0',
   i_default  => m_tap_config(i), 
   o_signal   => m_tap_config(i) 
  );
 
  tap_rbk: entity work.ipbus_register
  port map(
   c_address  => std_logic_vector(to_unsigned(AO_ADDR_TAPR+i,32)),
   i_clk      => i_clk_ipb,
   i_reset    => i_resetFifo,
   i_ipbw     => i_ipbw(AO_ADDR_TAPR+i),
   o_ipbr     => i_ipbr(AO_ADDR_TAPR+i),
   c_readonly => '1',
   i_default  => m_tap_rbk(i), 
   o_signal   => open 
  );  

 end generate SD_PACK;   
 o_test       <= m_data_delay0;
 --o_fastSignal <= m_sigOR(0);
 o_refBeforeFifo <= m_sigOR(0);
 
--------------------------------------------------------------------------------------------------------------
--building the full Malta word
 counter: entity work.SlowCounter
 port map( 
  i_clk   => m_clk_500MHz, 
  i_rst   => m_clk_40MHz,
  o_count => m_clkCounter
 );
 o_count_dbg <= m_clkCounter(2 downto 0);

 --this module takes 1 clk: need to pipeline m_sample by 1 clk
 position: entity work.EdgeDetector
 port map(
  i_clk      => m_clk_500MHz, 
  i_sample   => m_sample(0), 
  o_position => m_edgeCount,
  o_REP      => m_RefPin, 
  o_RE       => m_RefEnable 
 );
 o_debug1     <= m_fifo1_wrEn;
 o_fastSignal <= m_sigOR(0);
 process(m_clk_500MHz)
 begin
  if rising_edge(m_clk_500MHz) then
   m_sample_del <= m_sample;      
  end if;
 end process;

 m_maltaWord(0) <= m_RefEnable; --'1'; --first PIN is on by definition
 PIN_PACK : for I in 1 to NPINS-1 generate
-- PIN_PACK : for I in 1 to 2 generate   
  detector: entity work.PinDetector
  port map( 
   --i_clk       => m_clk_500MHz, 
   i_sample    => m_sample_del(I), 
   --i_re        => m_RefEnable, 
   i_sampleRef => m_RefPin,  
   o_result    => m_maltaWord(I)
  ); 
 end generate PIN_PACK;  

 --VD pipelining maltaword together with the edge for simplicity
 m_ToDelay(36 downto  0) <=  m_maltaWord;
 m_ToDelay(39 downto 37) <=  m_edgeCount;

--------------------------------------------------------------------------------------
-- programmable readout delay
tap_config: entity work.ipbus_register
port map(
  c_address  => std_logic_vector(to_unsigned(AO_ADDR_DELAY,32)),
  i_clk      => i_clk_ipb,
  i_reset    => i_resetFifo,
  i_ipbw     => i_ipbw(AO_ADDR_DELAY),
  o_ipbr     => i_ipbr(AO_ADDR_DELAY),
  c_readonly => '0',
  i_default  => m_Delay_Reg, 
  o_signal   => m_Delay_Reg 
);

m_Delay_Vec  <= m_Delay_Reg(7 downto 0);
--------------------------------------------------------------------------------------

 -- delays larger than 255 dont make sense since the delay chain is not long enough for more
 m_Delay_Int <= to_integer(unsigned(m_Delay_Vec));
 
 process(m_clk_500MHz)
 begin
  if rising_edge(m_clk_500MHz) then
    m_Delay(0) <= m_ToDelay;
    for i in 1 to 255 loop --was 99
        m_Delay(i) <= m_Delay(i-1);
    end loop;
    m_FromDelay<= m_Delay(m_Delay_Int);
  end if;
 end process;
 o_debug2 <= m_finalWord(0);

 m_finalWord(36 downto  0) <= m_FromDelay(36 downto  0);
 m_finalWord(39 downto 37) <= m_FromDelay(39 downto 37);  
 m_finalWord(42 downto 40) <= m_clkCounterL(2 downto 0);
 m_finalWord(43) <= '0';
 m_finalWord(44+TRIG_ID_SIZE-1 downto 44)  <= m_trigID_out;  -- bojan
 -----------o_refBeforeFifo <= m_finalWord(0); --VD last
 
 process(m_clk_500MHz)
  begin
   if rising_edge(m_clk_500MHz) then
    -- these 3 are evaluated together (all based on final word)
     m_isColOK <= (not m_finalWord(33)  or not i_RO_disable_HalfColumn);    
     m_isRowOK <= (not m_finalWord(21) or not i_RO_disable_HalfRow); 
     m_tmpWord <= m_finalWord;
     --  these 2 are evaluated togther (all based on the previous 3 inputs)
     --m_fifo1_wrEn <= m_tmpWord(0) and (not i_RO_disable) and m_isColOK and m_isRowOK and (not m_overflow);
     m_fifo1_wrEn <= m_tmpWord(0) and (not i_RO_disable) and (not m_overflow);
     m_tmpWord2 <= m_tmpWord;
  end if;
 end process;  
 --o_refBeforeFifo <= m_fifo1_wrEn;
 
 
 theFIFO1 : fifo_Fast
 port map(
  rst           => i_ResetFifo,
  wr_clk        => m_clk_500MHz,
  rd_clk        => m_clk_Idel, --is it a proble if I use the idelay clk?
  din           => m_tmpWord2, --m_finalWord,
  wr_en         => m_fifo1_wrEn,  
  rd_en         => m_fifo1_rdEn, 
  dout          => m_fifo1_out,
  full          => m_fifo1_full ,
  almost_full   => open, --m_fifo1_almost,
  prog_full     => m_fifo1_halffull,
  empty         => m_fifo1_empty
 );

 theFIFO2 : fifo_IPbus
 port map(
  rst           => i_ResetFifo,
  wr_clk        => m_clk_Idel,
  rd_clk        => i_clk_IPb,
  din           => m_fifo2_in,
  wr_en         => m_fifo2_wrEn,  
  rd_en         => m_fifo2_rdEn, 
  dout          => m_fifo2_out,
  full          => open, 
  almost_full   => open,
  empty         => open, 
  almost_empty  => open, --m_fifo2_empty,
  prog_full     => m_fifo2_full,
  prog_empty    => m_fifo2_empty, --m_fifo2_progE
  rd_data_count => m_fifo2_rdctr
 );
  
 connector: entity work.FifoConnector 
 port map ( 
  i_clk         => m_clk_Idel,
  i_fifo1_empty => m_fifo1_empty,
  i_fifo2_almost=> m_fifo2_full, 
  o_fifo1_rdEn  => m_fifo1_rdEn,
  o_fifo2_wrEn  => m_fifo2_wrEn, 
  i_fifo1_out   => m_fifo1_out,
  o_fifo2_in    => m_fifo2_in
 );

 o_fifo1_empty    <= m_fifo1_empty;
 o_fifo2_empty    <= m_fifo2_empty; 
 o_fifo2_ProgEmpty<= m_fifo2_progE;
 o_fifo1_Full     <= m_fifo1_full;  --m_fifo1_almost;
 o_fifo1_HalfFull <= m_fifo1_halffull;
 o_fifo2_Full     <= m_fifo2_full; --m_fifo2_almost;
 o_fifo2_rdCount  <= m_fifo2_rdctr;
 
 slaveFifo: entity work.ipbus_fifo
 generic map(
  c_ok_in_msb => '1'
 )
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_AOFIFO,32)), 
  i_clk      => i_clk_IPb,
  i_ipbw     => i_ipbw(IPBADDR_AOFIFO),
  o_ipbr     => i_ipbr(IPBADDR_AOFIFO),
  c_readonly => '1', 
  i_empty    => m_fifo2_empty, 
  i_full     => m_fifo2_full,
  i_rddata   => m_fifo2_out, 
  o_rdclk    => open,       
  o_rdenable => m_fifo2_rdEn, 
  o_wrclk    => open,
  o_wrdata   => open,        
  o_wrenable => open 
 );
 
--------------------------------------------------------------------------------------------------------------
-------monitoring object
-- aomon: entity work.AOMonitoring 
-- port map( 
--   i_clk_500MHz   => m_clk_500MHz,
--   i_clk_IPb      => i_clk_IPb,
--   i_ResetFifo    => i_ResetFifo,
--   i_fifoEnable   => m_mon_fifo_enable,
--   i_ipbw         => i_ipbw,
--   i_ipbr         => i_ipbr, 
--   i_Ready        => m_outReady, 
--   i_ORs          => m_outOR_2,
--   i_out          => m_out,
--   o_fifoM_empty  => o_fifoM_empty,
--   o_fifoM_full   => o_fifoM_Full,
--   i_RO_disable   => i_RO_disable,
--   i_RO_disable_HalfColumn => i_RO_disable_HalfColumn,
--   i_RO_disable_HalfRow    => i_RO_disable_HalfRow  
-- );
 
 m_mon_fifo_enable <= not i_RO_disable;

 --------------------------------------------------------------------------------------------------------------
 -- bojan trigID slow counter
  m_trigID_reset  <= i_trigID_reset;
  m_trigID_hold   <= i_trigID_hold;
  m_trigID_mode   <= i_trigID_mode;
  m_trigID_trig   <= i_trigID_trig;
  o_trigID_out    <= m_trigID_out;      -- for debugging
  
  trigID_counter_external : entity work.counter(external)
  generic map ( counter_bits => 12 ) --TRIG_ID_SIZE
  port map (
    i_clk           => m_clk_500MHz,
    i_reset         => m_trigID_reset,
    i_hold          => m_trigID_hold,
    i_mode_clocked  => m_trigID_mode,
    i_trig          => m_trigID_trig,
    o_counter       => m_trigID_out(17 downto 6)
  );
  
  m_LA1_reset <= m_trigID_reset or m_trigID_trig; --VD: this is actually the BCID reset !!!!
    
--  Clk40MHz_counter_internal : entity work.counter(internal)
--  generic map ( counter_bits => 6 )
--  port map (
--    i_clk          => m_clk_40MHz,
--    i_reset        => m_LA1_reset,
--    i_hold         => m_trigID_hold,
--    i_mode_clocked => m_trigID_mode,
--    i_trig         => m_trigID_trig,
--    o_counter      => open --m_trigID_out(5 downto 0)
--  );
--  m_FastClock_reset <= m_LA1_reset or m_clk_40MHz;
     
--  Clk320MHz_counter_internal : entity work.counter(internal)
--  generic map ( counter_bits => 3 ) 
--  port map (
--    i_clk           => m_clk_500MHz,
--    i_reset         => m_FastClock_reset,
--    i_hold          => m_trigID_hold,
--    i_mode_clocked  => m_trigID_mode,
--    i_trig          => m_trigID_trig,
--    o_counter       => m_clkCounter(2 downto 0)
--  );   
--  m_clkCounter(3) <= '0';

  Clk320MHz_counter_internal : entity work.counter(internal)
  generic map ( counter_bits => 9 ) 
  port map (
    i_clk           => m_clk_500MHz,
    i_reset         => m_LA1_reset,
    i_hold          => m_trigID_hold,
    i_mode_clocked  => m_trigID_mode,
    i_trig          => m_trigID_trig,
    i_maxCounterVal => i_maxCounterVal,
    o_counter       => m_clkCounterL,
    o_overflow      => m_overflow
  );   
  m_trigID_out(5 downto 0) <= m_clkCounterL(8 downto 3);
  

end Main;
