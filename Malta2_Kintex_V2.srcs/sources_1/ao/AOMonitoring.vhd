library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;

entity AOMonitoring is
 Port ( 
   i_clk_500MHz     : in  std_logic;
   i_clk_IPb        : in  std_logic;
   i_ResetFifo      : in  std_logic;
   i_fifoEnable     : in  std_logic;
   i_ipbw           : in     ipb_wbus_array(NSLAVES-1 downto 0);
   i_ipbr           : inout  ipb_rbus_array(NSLAVES-1 downto 0);
   i_Ready          : in  std_logic_vector(NPINS-1 downto 0);
   i_ORs            : in  std_logic_vector(NPINS-1 downto 0);
   i_out            : in  MatrixW;
   o_fifoM_empty    : out std_logic;
   o_fifoM_full     : out std_logic;
   i_RO_disable     : in  std_logic;
   i_RO_disable_HalfColumn : in  std_logic;
   i_RO_disable_HalfRow    : in  std_logic    
 );
end AOMonitoring;

architecture Main of AOMonitoring is

 component fifo_Vale is 
 port (
  rst           : IN STD_LOGIC;
  wr_clk        : IN STD_LOGIC;
  rd_clk        : IN STD_LOGIC;
  din           : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
  wr_en         : IN STD_LOGIC;
  rd_en         : IN STD_LOGIC;
  dout          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
  full          : OUT STD_LOGIC;
  almost_full   : OUT STD_LOGIC;
  empty         : OUT STD_LOGIC
 );
 end component fifo_Vale;

 signal m_fifo_writeEn: std_logic := '0';
 signal m_fifo_enable : std_logic := '0';
 signal m_fifo_readEn : std_logic := '0';
 signal m_fifo_empty  : std_logic := '1';
 signal m_fifo_full   : std_logic := '0';
 signal m_fifo_almost : std_logic := '0';
 signal m_fifo_def    : std_logic_vector(31 downto 0) := (others => '0');  
 signal m_fifo_out    : std_logic_vector(31 downto 0) := "00000000000000001111111111111111";
 signal m_pin_count   : std_logic_vector( 5 downto 0) := (others => '0');

 signal m_status      : natural range 0 to NPINS :=0;
 signal m_trigger     : std_logic := '0';

 signal m_latched     : MatrixW;
 signal m_isRowOK     : std_logic := '1';
 signal m_isColOK     : std_logic := '1';

begin

 m_fifo_enable <= i_fifoEnable;

 o_fifoM_empty <= m_fifo_empty;
 o_fifoM_full  <= m_fifo_almost;

 theFIFO : fifo_Vale
 port map(
  rst           => i_ResetFifo,
  wr_clk        => i_clk_500MHz,
  rd_clk        => i_clk_IPb,
  din           => m_fifo_def,
  wr_en         => m_fifo_writeEn,  
  rd_en         => m_fifo_readEn,
  dout          => m_fifo_out,
  full          => m_fifo_full ,
  almost_full   => m_fifo_almost,
  empty         => m_fifo_empty
 );

 slaveMonitor: entity work.ipbus_fifo
 generic map(
  c_ok_in_msb => '1'
 )
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_AOMONI,32)),
  i_clk      => i_clk_IPb, 
  i_ipbw     => i_ipbw(IPBADDR_AOMONI),
  o_ipbr     => i_ipbr(IPBADDR_AOMONI),
  c_readonly => '1', 
  i_empty    => m_fifo_empty, 
  i_full     => m_fifo_full,
  i_rddata   => m_fifo_out, 
  o_rdclk    => open,       
  o_rdenable => m_fifo_readEn, 
  o_wrclk    => open,
  o_wrdata   => open,        
  o_wrenable => open 
 );

 
 --m_trigger   <= i_Ready(0) and (not i_RO_disable) and (not i_ORs(33)) and (not i_ORs(21));
 --m_trigger   <= i_Ready(0) and (not i_RO_disable) and (not i_ORs(33));
 m_isColOK <= (not i_ORs(33) or not i_RO_disable_HalfColumn);    
 m_isRowOK <= (not (i_ORs(21) or i_ORs(20)) or not i_RO_disable_HalfRow);  
 
 m_trigger <= i_Ready(0) and (not i_RO_disable) and m_isColOK and m_isRowOK;
 
 m_pin_count <= conv_std_logic_vector(m_status,6);
 m_fifo_def(30 downto 25) <= m_pin_count;
 mainMonitor : process (i_clk_500MHz)
 begin
  if rising_edge(i_clk_500MHz) then
   if m_status=0 then
    if m_trigger='1' and m_fifo_enable = '1' then
     m_fifo_writeEn <= '1';
     m_latched <= i_out;  --caching the signal for all the time needed to write it to the FIFO!!!!
     m_fifo_def(23 downto 0) <= i_out(0);
     m_status <= m_status + 1;
    end if;
   elsif m_status=1 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1;
   elsif m_status=2 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1;
   elsif m_status=3 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1;
   elsif m_status=4 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1;  
   elsif m_status=5 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1;  
   elsif m_status=6 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);   
    m_status    <= m_status + 1;           
   elsif m_status=7 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=8 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=9 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=10 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=11 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=12 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=13 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=14 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=15 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=16 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=17 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=18 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=19 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=20 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=21 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=22 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=23 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=24 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=25 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=26 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=27 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=28 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=29 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=30 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=31 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=32 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=33 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=34 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=35 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   elsif m_status=36 then
    m_fifo_def(23 downto 0) <= m_latched(m_status);
    m_status    <= m_status + 1; 
   else
    m_fifo_writeEn <= '0';
    m_status <= 0;
   end if;   
  end if;
 end process; 

end Main;
