library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

--will probably have some sort of output trigger

entity EdgeDetector is
 Port (
  i_clk      : in  std_logic;
  i_sample   : in  std_logic_vector(7 downto 0); 
  o_position : out std_logic_vector(2 downto 0);
  o_REP      : out std_logic_vector(7 downto 0);
  o_RE       : out std_logic
 );
end EdgeDetector;

architecture Main of EdgeDetector is

 signal m_position : std_logic_vector(2 downto 0) := "000";
 
 signal m_II       : std_logic_vector(7 downto 0) := (others => '0');
 signal m_re       : std_logic                    := '0';
 signal m_re_pos   : std_logic_vector(7 downto 0) := (others => '0');

begin
   
   process(i_clk)
   begin
     if rising_edge(i_clk) then
       case i_sample is
-- VD: on 18-03-2016
-- assuming that the gap is at least 4
-- assuming that the signal length is 3-4-or 5
-- double triggering will be removed offline
-- REMOVING ALL THE CENTRAL INSTANCES OF 2

--- emergency adding 2-bin word    
         when "00000110"                                  => m_position <= "001"; m_re_pos <= "00001111"; m_re <= '1';  
         when "00001100"                                  => m_position <= "010"; m_re_pos <= "00011110"; m_re <= '1';
         when "00011000"                                  => m_position <= "011"; m_re_pos <= "00111100"; m_re <= '1';         
         when "00110000"                                  => m_position <= "100"; m_re_pos <= "01111000"; m_re <= '1';
         when "01100000"                                  => m_position <= "101"; m_re_pos <= "11110000"; m_re <= '1';

-- single word cases
         when "00000111"|"00001111"|"00011111"|"00111111"  => m_position <= "000"; m_re_pos <= "00001111"; m_re <= '1';       
         when "00001110"|"00011110"|"00111110"|"01111110"  => m_position <= "001"; m_re_pos <= "00011111"; m_re <= '1';  
         when "00011100"|"00111100"|"01111100"|"11111100"  => m_position <= "010"; m_re_pos <= "00111111"; m_re <= '1';
         when "00111000"|"01111000"|"11111000"             => m_position <= "011"; m_re_pos <= "01111110"; m_re <= '1';         
         when "01110000"|"11110000"                        => m_position <= "100"; m_re_pos <= "11111100"; m_re <= '1';
         when "11100000"                                   => m_position <= "101"; m_re_pos <= "11111000"; m_re <= '1';
         when "11000000"                                   => m_position <= "110"; m_re_pos <= "11110000"; m_re <= '1';
------------------         when "10000000"                                   => m_position <= "111"; m_re_pos <= "11110000"; m_re <= '1';
         
-- dealing with double words: easy cases
------------------         when "10000001"                                  => m_position <= "111"; m_re_pos <= "11110000"; m_re <= '1'; 
------------------         when "10000011"                                  => m_position <= "111"; m_re_pos <= "11110000"; m_re <= '1'; 
         when "11000001"                                  => m_position <= "110"; m_re_pos <= "11110000"; m_re <= '1'; 
         when "11000011"                                  => m_position <= "110"; m_re_pos <= "11110000"; m_re <= '1'; 
         when "11100001"                                  => m_position <= "101"; m_re_pos <= "11111000"; m_re <= '1'; 
         
-- dealing with the only really ambiguous case (the only wayout to catch all the cases is to start triggering on 2 and rely on the double triggering removal)
         when "00000011"                                  => m_position <= "000"; m_re_pos <= "00001111"; m_re <= '1';       
------------------         when "10000111"                                  => m_position <= "000"; m_re_pos <= "00001111"; m_re <= '1';

-- rest          
         when others                                      => m_position <= "000"; m_re_pos <= (others => '0'); m_re <= '0';
       end case;
     end if;
   end process;

 o_position <= m_position; 
 o_REP      <= m_re_pos;
 o_RE       <= m_re; 
end Main;



--         when "00001111"                                  => m_position <= "000"; m_re_pos <= "00000001"; m_re <= '1';       
--         when "00011110"                                  => m_position <= "001"; m_re_pos <= "00000010"; m_re <= '1';  
--         when "00111100"                                  => m_position <= "010"; m_re_pos <= "00000100"; m_re <= '1';
--         when "01111000"                                  => m_position <= "011"; m_re_pos <= "00001000"; m_re <= '1';         
--         when "11110000"                                  => m_position <= "100"; m_re_pos <= "00010000"; m_re <= '1';

--         when "11100000"|"11100001"                       => m_position <= "101"; m_re_pos <= "00100000"; m_re <= '1';
--         when "11000000"|"11000001"|"11000011"            => m_position <= "110"; m_re_pos <= "01000000"; m_re <= '1';
--         when "10000000"|"10000001"|"10000011"|"10000111" => m_position <= "111"; m_re_pos <= "10000000"; m_re <= '1'; 
--         when others                                      => m_position <= "000"; m_re_pos <= (others => '0'); m_re <= '0';