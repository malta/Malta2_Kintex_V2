----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/02/2018 10:46:51 AM
-- Design Name: 
-- Module Name: Malta_Monitoring - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VComponents.all;

use work.ipbus.all;
use work.readout_constants.all;

entity Malta_Monitoring is
  Port ( 
-- connections from / to internal
    i_ClkToMalta          : in std_logic;
    i_RST_TOMALTA         : in std_logic;
    i_vpulse_toMalta      : in std_logic;
    i_TRIG_TOMALTA        : in std_logic;
    i_SC_toMalta          : in std_logic;
    i_LOAD_toMalta        : in std_logic;
    
-- connections from / to external
    o_CLK_TOMALTA_P       : out std_logic;
    o_CLK_TOMALTA_N       : out std_logic;                
    o_RST_TOMALTA         : out std_logic;
    o_VPULSE_TOMALTA_P    : out std_logic;
    o_VPULSE_TOMALTA_N    : out std_logic;
    o_TRIG_TOMALTA_P      : out std_logic;
    o_TRIG_TOMALTA_N      : out std_logic;
    o_SC_TOMALTA_P        : out std_logic;
    o_SC_TOMALTA_N        : out std_logic;
    o_LOAD_toMalta        : out std_logic
    
  );
end Malta_Monitoring;

architecture Behavioral of Malta_Monitoring is

 --signal m_Pixels_monitor : std_logic_vector(1 downto 0);
 signal m_TMPCLK  : std_logic;
 signal m_TMPLOAD : std_logic;

begin

---------------------------------------------------------------------------------------------------------------------------- 
-- clkOut: ODDR
-- port map (
--    Q  => m_TMPCLK,
--    C  => i_ClkToMalta,
--    CE => '1', 
--    D1 => '1',
--    D2 => '0'
--    --.R(R),   // 1-bit reset
--    --.S(S)    // 1-bit set
-- ); 

  OBUFDS_clk : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
  port map (
    O  => o_CLK_TOMALTA_P,
    OB => o_CLK_TOMALTA_N,
    I  => i_ClkToMalta --m_TMPCLK --i_ClkToMalta
  );  

---------------------------------------------------------------------------------------------------------------------------- 
  OBUF_reset : OBUF
  generic map (
    IOSTANDARD => "DEFAULT",
    SLEW       => "SLOW",
    DRIVE      => 4 )
  port map (
    O => o_RST_TOMALTA,
    I => i_RST_TOMALTA
  );
 
---------------------------------------------------------------------------------------------------------------------------- 
  OBUFDS_VPulse : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT",
     SLEW       => "SLOW")
  port map (
    O  => o_VPULSE_TOMALTA_P,
    OB => o_VPULSE_TOMALTA_N,
    I  => i_vpulse_toMalta 
  );
   
-------------------------------------------------------------------------------------------------------------------------- 
  OBUFDS_Trigger : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT",  
    SLEW       => "SLOW")
  port map (
    O  => o_TRIG_TOMALTA_P,
    OB => o_TRIG_TOMALTA_N,
    I  => i_TRIG_TOMALTA
  );

--------------------------------------------------------------------------------------------------------------------------   
  OBUFDS_SC_OUT : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT", 
    SLEW       => "FAST")    
  port map (
    O  => o_SC_TOMALTA_P,
    OB => o_SC_TOMALTA_N,
    I  => i_SC_toMalta
  );     
  
---------------------------------------------------------------------------------------------------------------------------- 
---------------------------------------------------------------------------------------------------------------------------- 
  OBUF_load : OBUF
  generic map (
    IOSTANDARD => "DEFAULT",
    SLEW       => "SLOW",
    DRIVE      => 4)
  port map (
    O => o_LOAD_toMalta,
    I => m_TMPLOAD --i_LOAD_toMalta
  );
  m_TMPLOAD <= i_LOAD_toMalta; --and i_ClkToMalta;-- and (not i_ClkToMalta);
  --o_LOAD_toMalta      <= i_LOAD_toMalta; 

end Behavioral;
