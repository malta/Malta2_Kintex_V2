-------------------------------------------------------------------------
-- MALTA Readout Top File
-- 
-- Includes 
--  SlowControl
--  Asynch Oversampling
--  IPBus
-- 
-- Valerio.Dao@cern.ch
-- Ignacio ....
------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;


entity Top_Readout is
 Port (
  SYSCLK_P          : in std_logic;
  SYSCLK_N          : in std_logic;
  
--  i_USER_SMA_CLOCK_P : in std_logic;
--  i_USER_SMA_CLOCK_N : in std_logic;
  
  i_GPIO_SW_N         : in std_logic;
  i_GPIO_SW_S         : in std_logic;
  
  --official input FMC
  i_FMC_PIN_P       : in std_logic_vector(NPINS-1 downto 0); 
  i_FMC_PIN_N       : in std_logic_vector(NPINS-1 downto 0);
  
  -- external clock
--  i_SMA_EXT_CLK_P   : in std_logic;
--  i_SMA_EXT_CLK_N   : in std_logic;
  
  --SC pins
  o_TRIG_TOMALTA_P   : out std_logic;
  o_TRIG_TOMALTA_N   : out std_logic;
  o_VPULSE_TOMALTA_P : out std_logic;
  o_VPULSE_TOMALTA_N : out std_logic;
  o_RST_TOMALTA      : out std_logic;
  
  --for IPBUS
  SGMIICLK_Q0_P     : in  std_logic;
  SGMIICLK_Q0_N     : in  std_logic;
  SGMII_TX_P        : out std_logic;
  SGMII_TX_N        : out std_logic;
  SGMII_RX_P        : in  std_logic;
  SGMII_RX_N        : in  std_logic;
  PHY_RESET         : out std_logic; 
  
  --for handling MALTA
  o_VREF           : out std_logic;      
  
  --for monitoring
  i_SMA_user_P      : in std_logic;  --was out
  o_SMA_user_N      : out std_logic;
  i_swi             : in  std_logic_vector(2 downto 0);
  o_LED             : out std_logic_vector(7 downto 0);
  
--  --external clock
--  i_EXT_CLK_P       : in std_logic;
--  i_EXT_CLK_N       : in std_logic;
  
  --new SC
  o_SC_LOAD         : out std_logic; 
  o_SC_TOMALTA_P    : out std_logic;
  o_SC_TOMALTA_N    : out std_logic;
  i_SC_FROMMALTA_P  : in  std_logic;
  i_SC_FROMMALTA_N  : in  std_logic;
  o_SC_CLK_P        : out std_logic;
  o_SC_CLK_N        : out std_logic;
  
  --FMC2 LEDs
  o_FMC2_LED        : out std_logic_vector (9 downto 0);
  o_FMC2_SMA        : out std_logic_vector (9 downto 0)
  
 );
 
end Top_Readout;

architecture Main of Top_Readout is

--first all the IPs
 component Clk_Provider is 
 port(
  clk_out1  : out std_logic  :='0';
  clk_out2  : out std_logic  :='0';
  clk_out3  : out std_logic  :='0';
  clk_out4  : out std_logic  :='0';
  reset     : in  std_logic  :='0';
  locked    : out std_logic  :='0';
  clk_in1   : in  std_logic  :='0'
 );
 end component Clk_Provider;

--first all the IPs
 component Clk_Provider2 is 
 port(
  clk_out1  : out std_logic  :='0';
  clk_out2  : out std_logic  :='0';
  reset     : in  std_logic  :='0';
  locked    : out std_logic  :='0';
  clk_in1   : in  std_logic  :='0'
 );
 end component Clk_Provider2;
 
 component Clk_Provider3 is 
 port(
  clk_out1  : out std_logic  :='0';
  clk_out2  : out std_logic  :='0';
  clk_out3  : out std_logic  :='0';
  reset     : in  std_logic  :='0';
  locked    : out std_logic  :='0';
  clk_in1_p : in  std_logic  :='0';
  clk_in1_n : in  std_logic  :='0'
 );
 end component Clk_Provider3;

-- Dummy, Status, Reset and Busy
 signal m_dummy   : std_logic_vector(31 downto 0) := X"20210406";
 signal m_status  : std_logic_vector(31 downto 0) := X"20180406";
 signal m_reset   : std_logic_vector(31 downto 0) := X"00000000";
 signal m_resetSt : natural range 0 to 10 := 0;
 signal m_resetIP : std_logic :='0';
 signal m_busy    : std_logic_vector(31 downto 0) := (others => '0');
 signal m_sys_rst_inverted : std_logic := '0';
 signal m_sys_rst_TMP      : std_logic := '0';
 signal m_sys_rst_TMP2     : std_logic := '1';
 signal m_enable_all       : std_logic := '0';
 signal m_testCesar        : std_logic_vector (NPINS-1 downto 0) := (others =>'0');--: std_logic := '0'; 
 signal m_counter          : integer := 40;
 signal m_testCesar_del    : std_logic_vector (2 downto 0) := (others =>'0');--: std_logic := '0'; 
 signal m_finalDebug       : std_logic := '0'; 
 
-- only main clks defined here
 signal m_clk_500MHz     : std_logic;
 signal m_clk_500MHz_90  : std_logic;
 signal m_clk_200MHz_idel: std_logic;
 signal m_clk_200MHz_ip  : std_logic;
 signal m_clk_40MHz      : std_logic;     
 signal m_clk_10MHz      : std_logic;
 signal m_clk_ToMalta    : std_logic;
 signal m_clk_Slow       : std_logic;     
 signal m_clk_IPbus      : std_logic;
 signal m_extClk_320MHz  : std_logic;
 signal m_extClk_b_320MHz: std_logic;
 signal m_extClk_40MHz   : std_logic;

-- IPbus variables
 signal m_sys_rst        : std_logic := '0';
 signal m_ipbw           : ipb_wbus_array(NSLAVES-1 downto 0);
 signal m_ipbr           : ipb_rbus_array(NSLAVES-1 downto 0);

-- for monitoring
 signal m_led            : std_logic_vector( 7 downto 0) := (others => '0');
 
-- signals for trigger
 signal m_L1Ainput_ff_p     : std_logic := '0';
 signal m_L1Ainput_ff_n     : std_logic := '0';
 signal m_L1Ainput_ff_n_ff  : std_logic := '0';
 signal trig                : std_logic := '0';
 signal trig_del            : std_logic_vector(7 downto 0) := (others => '0');
  
-- for communicatoin with dummy sender
 signal m_Dummy_Command  : std_logic_vector(IPBUS_WIDTH-1 downto 0) := (others => '0');  --general purpose communication string
 signal m_Delay_Command  : std_logic_vector(IPBUS_WIDTH-1 downto 0) := (others => '0');  
 signal m_Delay_Counter  : std_logic_vector(5 downto 0) := (others => '0');  
 signal m_Dummy_Push     : std_logic := '0';
 signal m_Dummy_Reset    : std_logic := '0';
 signal m_Dummy_Swi_1    : std_logic := '0'; 
 signal m_Dummy_Swi_2    : std_logic := '0'; 
 signal m_Dummy_Swi_3    : std_logic := '0'; 
 signal m_Dummy_Swi_4    : std_logic := '0';
 signal m_Dummy_Swi_5    : std_logic := '0';  

-- this is for AO module needs
 signal m_AOreset        : std_logic := '0'; 
 signal m_AOfifo1_empty  : std_logic := '1';
 signal m_AOfifo2_empty  : std_logic := '1';
 signal m_AOfifoM_empty  : std_logic := '1';
 signal m_AOfifo1_almost : std_logic := '0';
 signal m_AOfifo1_half   : std_logic := '0'; ---prog full
 signal m_AOfifo2_almost : std_logic := '0';
 signal m_AOfifo2_half   : std_logic := '0'; ---prog empty
 signal m_AOfifo2_rdCtr  : std_logic_vector(15 downto 0) := (others => '0');
 signal m_AOfifoM_almost : std_logic := '0';

-- Slow Control 
 signal m_trig_toMalta   : std_logic := '0';
 signal m_vpulse_toMalta : std_logic := '0';

 signal m_trigger  : std_logic:='0';
 signal m_TMPsysCLK: std_logic:='0';
  
 signal m_MaltaPushReset    : std_logic:='0';
 signal m_MaltaSC_ClkEnable : std_logic:='0';
 signal m_MaltaHighImpedance: std_logic:='1';
 signal m_ClkToMalta        : std_logic:='0';
 signal m_clk_10MHz_Delay1  : std_logic;
 signal m_clk_10MHz_Delay2  : std_logic;
 
 signal m_L1Atmp      : std_logic:='0';
 signal m_L1A         : std_logic:='0';
 signal m_L1AtoPico   : std_logic:='0';
 signal m_L1AtoMalta  : std_logic:='0';
 signal m_L1Afull     : std_logic:='0';
 signal m_L1Ainput    : std_logic:='0';
 signal m_L1A_selector: std_logic:='0';
 signal m_L1Adebug    : std_logic:='0';


 signal m_RO_disable            : std_logic:='0';
 signal m_RO_disable_HalfColumn : std_logic:='0';
 signal m_RO_disable_HalfRow    : std_logic:='0';
 
 signal m_DataLinesIn : std_logic_vector(40 downto 0) := (others => '0');
 
 signal m_noisyPixels_monitor : std_logic_vector (1 downto 0);
 
 -- Bojan counter
 signal m_counter_out     : std_logic_vector(TRIG_ID_SIZE-1 downto 0)  := (others => '0');
 signal m_slowCounter_dbg : std_logic_vector(2 downto 0)  := (others => '0');
 signal m_maxCounterVal   : std_logic_vector (8 downto 0);
 
 signal m_fastSignal      : std_logic:='0';
 signal m_fastSignalLong  : std_logic:='0';
 signal m_counterFast     : natural range 0 to 31 :=0;
 signal m_fastEnable      : std_logic:='0';
 
 signal m_debug1          : std_logic:='0';
 signal m_debug2          : std_logic:='0';
 signal m_clipper_deb1    : std_logic:='0';
 signal m_clipper_deb2    : std_logic:='0';
 
 -- pico TDC test
 signal m_toTDC           : std_logic_vector(3 downto 0):= (others => '0');
 
 -- IPB FIFO word count
 signal m_fifo2_wordCount : std_logic_vector(31 downto 0):= (others => '0');
 
 -- for new SC
 signal m_reading         : std_logic:='0';
 signal m_toMalta         : std_logic:='0';
 signal m_SC_fromMalta    : std_logic:='0';
 signal m_SC_ToMalta      : std_logic:='0';
 signal m_SC_Load         : std_logic:='0';
 
begin

---------------------------------------------------------------------------------------------------------------------------------------------------
-- System reset: North button or reset register bit 0
 m_sys_rst <= i_GPIO_SW_N or m_reset(0);
 PHY_RESET <= not m_sys_rst ;
 m_sys_rst_TMP  <= m_sys_rst_TMP2 and (i_GPIO_SW_S or m_MaltaPushReset);
 m_sys_rst_inverted <= not m_sys_rst_TMP;

 -- input clk
 inL: IBUFGDS
 port map( O=>m_TMPsysCLK, I=>SYSCLK_P, IB=>SYSCLK_N);
 
 OBUF_VREF: OBUF
 generic map (
   DRIVE => 12,
   IOSTANDARD => "DEFAULT",
   SLEW => "SLOW")
 port map (
   O => o_VREF,
   I => '1'
 );
 
---------------------------------------------------------------------------------------------------------------------------------------------------
-- main clk object
 mainClk1 : Clk_Provider
 port map(
  reset=>'0', locked=>open,
  clk_out1  => m_clk_500MHz, 
  clk_out2  => m_clk_500MHz_90,  
  clk_out3  => m_clk_40MHz, 
  clk_out4  => m_clk_10MHz, 
  clk_in1   => m_TMPsysCLK
 );

 mainClk2 : Clk_Provider2
 port map(
  reset=>'0', locked=>open,
  clk_out1  => m_clk_200MHz_idel, 
  clk_out2  => m_clk_200MHz_ip,
  clk_in1   => m_TMPsysCLK
 );
 

---------------------------------------------------------------------------------------------------------------------------------------------------
-- IPBUS top
 ipbus: entity work.ipbus_top
 generic map(NSLV => NSLAVES )
 port map(
  ipb_to_slaves   => m_ipbw,
  ipb_from_slaves => m_ipbr,
  i_clk200        => m_clk_200MHz_ip,
  o_clk1hz        => open, --m_led(0),
  o_clk_locked    => open, --m_led(0),
  o_eth_locked    => open, --m_led(7),
  i_addrSwi       => i_swi,
  o_ipb_clk       => m_clk_IPbus,
  i_sys_rst       => m_sys_rst,
  o_ipb_rx_led    => open, --m_led(2),
  o_ipb_tx_led    => open, --m_led(4),
  i_gt_clkp       => SGMIICLK_Q0_P,
  i_gt_clkn       => SGMIICLK_Q0_N,
  o_gt_txp        => SGMII_TX_P,
  o_gt_txn        => SGMII_TX_N,
  i_gt_rxp        => SGMII_RX_P,
  i_gt_rxn        => SGMII_RX_N
 );

 -- Register 0: Version (R)
 dummy: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_VERSION,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_VERSION),
  o_ipbr     => m_ipbr(IPBADDR_VERSION),
  c_readonly => '1',
  i_default  => m_dummy,
  o_signal   => open
 );
  
-- -- Register 1: Status (R)
-- status: entity work.ipbus_register
-- port map(
--  c_address  => std_logic_vector(to_unsigned(IPBADDR_STATUS,32)),
--  i_clk      => m_clk_IPbus,
--  i_reset    => m_sys_rst,
--  i_ipbw     => m_ipbw(IPBADDR_STATUS),
--  o_ipbr     => m_ipbr(IPBADDR_STATUS),
--  c_readonly => '1',
--  i_default  => m_status,
--  o_signal   => open
-- );
  
-----  Register 2: Reset (W)
-- reset: entity work.ipbus_register
-- port map(
--  c_address  => std_logic_vector(to_unsigned(IPBADDR_RESET,32)),
--  i_clk      => m_clk_IPbus,
--  i_reset    => m_sys_rst,
--  i_ipbw     => m_ipbw(IPBADDR_RESET),
--  o_ipbr     => m_ipbr(IPBADDR_RESET),
--  c_readonly => '0',
--  i_default  => X"00000000",
--  o_signal   => m_reset
-- );
  
 
 --Need a 2 state approach
-- process(m_clk_10MHz)
-- begin
-- if rising_edge(m_clk_10MHz) then
--  if m_reset(0)='1' and m_reset(1)='0' and m_resetSt=0 then --enable when the signal rise
--   m_resetIP <= '1';
--   m_resetSt <= 1;
--  elsif m_resetSt=1 then
--   --m_resetIP <= '0';
--   m_resetSt <= 2;
--  elsif m_resetSt>=2 and m_resetSt<10  then
--   m_resetSt <= m_resetSt+1;
--  elsif m_resetSt=10 then --and m_reset(1)='1' and m_reset(0)='0' then   
--   m_resetIP <= '0'; 
--   m_resetSt <= m_resetSt+1;
--   --m_resetSt <= 0;
--  end if;
  
--  if m_reset(0)='0' and m_reset(1)='1' then
--   m_resetSt <= 0;
--  end if;
  
-- end if;
-- end process;
 
 -- Register 3: Busy (R)
 busy: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_BUSY,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_BUSY),
  o_ipbr     => m_ipbr(IPBADDR_BUSY),
  c_readonly => '1',
  i_default  => m_busy,
  o_signal   => open
 );
  
 Slow_Control: entity work.MALTA2SlowControl
 port map( 
   o_SC_load       => m_SC_load,
   o_reading        => open, -- m_reading, --Only for debug
   o_SCclk_enable   => open, --m_MaltaSC_ClkEnable, --VD_enable_SCCLK,  -- Enables sending slock control clock
   o_FROclk_enable  => open, -- m_enable_FROCLK,
   o_SROclk_enable  => open, -- m_enable_SROCLK,
   o_SLOW_enable    => open, -- m_slow_flag,
   o_IPB_L1A        => open, -- m_ipb_L1, --VD: temporary
   -- slow control ports
   i_clk_sc         => m_clk_10MHz,          -- Connect a 40MHz clock
   i_sout           => m_SC_fromMalta,       -- Values of all registers as a serial stream from Malta2
   --o_rstB           => open,                 -- If '0' no input data is taken by miniMalta and the config of the chip is set to default
   o_data_toMalta   => m_SC_ToMalta,         -- Serial stream of 4321 bits sent to MiniMalta
   -- ipbus signals
   i_IPBeset        => m_sys_rst,
   i_clk_ipb        => m_clk_IPbus,          -- Connect a 32MHz clock   
   i_ipbw           => m_ipbw,
   i_ipbr           => m_ipbr,
   o_LPC_LEDS       => o_FMC2_LED,--open,--opm_LPC_LEDS ,-- VD: temporary
   o_LPC_SMA        => open  --m_LPC_SMA  ,-- VD: temporary
 );

 
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 m_L1Ainput  <= ( (not m_L1A_selector) and m_Dummy_Command(19)) or (m_L1A_selector and i_SMA_user_P);     --THIS IS FOR HSIO
 
 clipperToPico: entity work.L1AClipper(picoTDC)
 --clipperToPico: entity work.L1AClipper(picoTDCold)
 port map( 
  i_clk_fast    => m_clk_500MHz,
  i_clk_slow    => m_Clk_40MHz,--m_extClk_40MHz, 
  i_sig         => m_L1A,
  o_sig         => m_L1AtoPico,
  o_debug1      => m_clipper_deb1,
  o_debug2      => m_clipper_deb2
 );
 
 clipperToMalta: entity work.L1AClipper(maltaAO)
 port map( 
  i_clk_fast    => m_clk_500MHz,
  i_clk_slow    => m_Clk_40MHz,--m_extClk_40MHz, 
  --i_clk_slow    => m_extClk_40MHz, 
  i_sig         => m_L1Ainput,
  o_sig         => m_L1A
 );
 
 clipperToMaltadebug: entity work.L1AClipper(maltaAOdebug)
 port map( 
  i_clk_fast    => m_clk_500MHz,
  i_clk_slow    => m_Clk_40MHz,--m_extClk_40MHz, 
  --i_clk_slow    => m_extClk_40MHz, 
  i_sig         => m_L1Ainput,
  o_sig         => m_L1Adebug
 );
  
  
--  AO module : Registers 6 and 7
  ao: entity work.AOTop
  port map(  
   i_clk_500MHz     => m_clk_500MHz, 
   i_clk_500MHz_90  => m_clk_500MHz_90,  
   i_clk_Idel       => m_clk_200MHz_idel,  
   i_clk_IPb        => m_clk_IPbus,
   i_clk_40MHz      => m_clk_40MHz,
   i_ResetFifo      => m_AOReset,
   i_ipbw           => m_ipbw,
   i_ipbr           => m_ipbr,
   i_PIN_P          => i_FMC_PIN_P, 
   i_PIN_N          => i_FMC_PIN_N,
   o_test           => m_testCesar,
   o_debug1         => m_debug1,
   o_debug2         => m_debug2,
   o_refBeforeFifo  => m_FinalDebug,
   o_fastSignal     => m_fastSignal,
   --o_test2         => m_testCesar2,
   o_fifo1_empty    => m_AOfifo1_empty,
   o_fifo2_empty    => m_AOfifo2_empty,
   o_fifoM_empty    => m_AOfifoM_empty,
   o_fifo1_Full     => m_AOfifo1_almost,
   o_fifo1_HalfFull => m_AOfifo1_half,
   o_fifo2_ProgEmpty=> m_AOfifo2_half,
   o_fifo2_rdCount  => m_AOfifo2_rdCtr,
   o_fifo2_Full     => m_AOfifo2_almost,
   o_fifoM_Full     => m_AOfifoM_almost,
   -- bojan slow counter for trigger ID
   i_trigID_reset   => m_Dummy_Command(16), 
   i_trigID_hold    => m_Dummy_Command(17), 
   i_trigID_mode    => m_Dummy_Command(18), 
   i_trigID_trig    => m_L1Afull, 
   o_trigID_out     => m_counter_out,
   o_count_dbg      => m_slowCounter_dbg,
   i_Delay_Counter  => m_Delay_Counter,
   i_maxCounterVal  => m_maxCounterVal,
   i_RO_disable            => m_RO_disable,
   i_RO_disable_HalfColumn => m_RO_disable_HalfColumn,
   i_RO_disable_HalfRow    => m_RO_disable_HalfRow
  );
  o_SMA_user_N <=  m_testCesar(0); ---m_fastSignalLong and m_fastEnable;

  m_busy(0)  <= m_AOfifo1_almost;
  m_busy(1)  <= m_AOfifo2_almost;
  m_busy(2)  <= m_AOfifoM_almost;
  m_busy(3)  <= not m_AOfifo1_empty;
  m_busy(4)  <= not m_AOfifo2_empty;
  m_busy(5)  <= not m_AOfifoM_empty;
  m_busy(6)  <= m_AOfifo1_half;
  m_busy(11) <= m_AOfifo2_half;
  m_busy(31 downto 20) <= m_counter_out(17 downto 6);    -- Bojan
  
  process (m_clk_500MHz) is begin 
    if (rising_edge(m_clk_500MHz)) then
      if m_counterFast=0 then
        if m_fastSignal='1' then
          m_counterFast <=  m_counterFast+1;
          m_fastSignalLong <= '1';
        end if;
      elsif m_counterFast<2 then --was 4 length of signal to TLU 6.25 ns
        m_fastSignalLong <= '1';
        m_counterFast <=  m_counterFast+1;
      else
        m_fastSignalLong <= '0'; 
        m_counterFast <= 0;
      end if; 
    end if; 
  end process; 
  
  m_L1Afull <= m_L1A;
  m_fastEnable <= m_Delay_Command(6);

--Modified by Leyre to have the MALTA2 slowcontrol LOAD it was m_L1Ainput
 o_FMC2_SMA(0) <= m_SC_Load;  --m_testCesar(0); m_L1Ainput;
 o_FMC2_SMA(1) <= m_L1Adebug;
 o_FMC2_SMA(2) <= m_clk_ToMalta;
 o_FMC2_SMA(4) <= m_testCesar(0);
 o_FMC2_SMA(6) <= m_SC_ToMalta;
 o_FMC2_SMA(8) <= m_SC_fromMalta;
     
  
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 -- Delay control
 delayCommand: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_DELAY,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_DELAY),
  o_ipbr     => m_ipbr(IPBADDR_DELAY),
  c_readonly => '0',
  i_default  => m_Delay_Command, 
  o_signal   => m_Delay_Command
 );
 m_Delay_Counter <= m_Delay_Command(5 downto 0);
 m_maxCounterVal <= m_Delay_Command(18 downto 10);
 
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 -- Delay control
 FIFO2_WordCount: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(AO_ADDR_WDCTR,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(AO_ADDR_WDCTR),
  o_ipbr     => m_ipbr(AO_ADDR_WDCTR),
  c_readonly => '1',
  i_default  => m_fifo2_wordCount, 
  o_signal   => open
 );
 m_fifo2_wordCount(31 downto 16) <= (others => '0');
 m_fifo2_wordCount(15 downto  0) <= m_AOfifo2_rdCtr;
 
 ---------------------------------------------------------------------------------------------------------------------------------------------------
 -- DummySender control
 slaveCommand: entity work.ipbus_register
 port map(
  c_address  => std_logic_vector(to_unsigned(IPBADDR_DUMCTR,32)),
  i_clk      => m_clk_IPbus,
  i_reset    => m_sys_rst,
  i_ipbw     => m_ipbw(IPBADDR_DUMCTR),
  o_ipbr     => m_ipbr(IPBADDR_DUMCTR),
  c_readonly => '0',
  i_default  => m_Dummy_Command, 
  o_signal   => m_Dummy_Command
 );
 m_Dummy_Push   <= m_Dummy_Command(0);
 m_Dummy_Reset  <= m_Dummy_Command(1);
 m_AOReset      <= m_Dummy_Command(2) or m_sys_rst;
 m_Dummy_Swi_1  <= m_Dummy_Command(3);
 m_Dummy_Swi_2  <= m_Dummy_Command(4);
 m_Dummy_Swi_3  <= m_Dummy_Command(5);
 m_Dummy_Swi_4  <= m_Dummy_Command(6);
 m_Dummy_Swi_5  <= m_Dummy_Command(7);

 m_MaltaPushReset    <= m_Dummy_Command(12);
 m_vpulse_toMalta    <= m_Dummy_Command(20);
 m_L1A_selector      <= m_Dummy_Command(18);
  
 m_RO_disable            <= m_Dummy_Command(29);
 m_RO_disable_HalfColumn <= m_Dummy_Command(30);
 m_RO_disable_HalfRow    <= m_Dummy_Command(31); 
 
---------------------------------------------------------------------------------------------------------------------------------------------------
 --m_SC_fromMalta <='0';
  IBUFDS_SC_IN: IBUFDS
  port map(
   O =>m_SC_fromMalta,
   I =>i_SC_FROMMALTA_P,
   IB=>i_SC_FROMMALTA_N
  );
 m_clk_ToMalta <= m_clk_10MHz;
 Monitoring: entity work.Malta_Monitoring
 port map ( 
     i_ClkToMalta        => m_clk_ToMalta,
     i_RST_TOMALTA       => m_sys_rst_inverted,
     i_vpulse_toMalta    => m_vpulse_toMalta,    
     i_TRIG_TOMALTA      => m_trig_toMalta,
     i_SC_toMalta        => m_SC_ToMalta,
     i_LOAD_toMalta      => m_SC_Load,
     o_CLK_TOMALTA_P     => o_SC_CLK_P,
     o_CLK_TOMALTA_N     => o_SC_CLK_N,                
     o_RST_TOMALTA       => o_RST_TOMALTA,
     o_VPULSE_TOMALTA_P  => o_VPULSE_TOMALTA_P,
     o_VPULSE_TOMALTA_N  => o_VPULSE_TOMALTA_N,
     o_TRIG_TOMALTA_P    => o_TRIG_TOMALTA_P,
     o_TRIG_TOMALTA_N    => o_TRIG_TOMALTA_N,
     o_SC_TOMALTA_P      => o_SC_TOMALTA_P,
     o_SC_TOMALTA_N      => o_SC_TOMALTA_N,
     o_LOAD_toMalta      => o_SC_LOAD   
 );

---------------------------------------------------------------------------------------------------------------------------------------------------------------- 
 
 m_led(0) <= m_L1A_selector;
 m_led(1) <= m_Dummy_Command(16);
 m_led(2) <= i_swi(0);
 m_led(3) <= i_swi(1);
 m_led(4) <= i_swi(2);

 o_led <= m_led;
 
end Main;
