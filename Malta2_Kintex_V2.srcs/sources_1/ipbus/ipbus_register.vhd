--------------------------------------------------------
-- IPBUS Register
--
-- This is not part of the IPbus distribution
-- Carlos.Solans@cern.ch
-- November 2017
--------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ipbus.all;

entity ipbus_register is

	port(
	    c_address  : in std_logic_vector(31 downto 0) := (others => '0'); --the register's address
    	i_clk      : in std_logic;                     --ipbus clock
		i_reset    : in std_logic;                     --reset to default value
		i_ipbw     : in ipb_wbus;                      --ipbus write bus
		o_ipbr     : out ipb_rbus;                     --ipbus read bus
		c_readonly : in std_logic;                     --high if register is read only
		i_default  : in std_logic_vector(31 downto 0) := (others => '0'); --default value at reset
		o_signal   : out std_logic_vector(31 downto 0) --signal to read and write
	);
	
end ipbus_register;

architecture rtl of ipbus_register is

	signal ack: std_logic;
	signal reg: std_logic_vector(31 downto 0);
	
begin

    o_ipbr.ipb_addr <= c_address;
	o_ipbr.ipb_ack <= ack;
    o_ipbr.ipb_err <= '0';
	
	process(i_clk,i_default)
	begin
		if rising_edge(i_clk) then
			if i_reset='1' then
				reg <= i_default;
			elsif c_readonly='1' then
				reg <= i_default;
			elsif i_ipbw.ipb_strobe='1' and i_ipbw.ipb_write='1' then
				reg <= i_ipbw.ipb_wdata;
				
			end if;

			if c_readonly = '1' then
				o_ipbr.ipb_rdata <= i_default;
			else
				o_ipbr.ipb_rdata <= reg; 
			end if;
			
			ack <= i_ipbw.ipb_strobe and not ack;
			o_signal <= reg;
		end if;
	end process;	
	
end rtl;

