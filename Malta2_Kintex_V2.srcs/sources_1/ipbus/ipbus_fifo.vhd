--------------------------------------------------------
-- IPBUS FIFO
--
-- This is not part of the IPbus distribution
-- Carlos.Solans@cern.ch
-- November 2017
--
-- Write to the FIFO with the clock of the IPBus when 
--  IPBUS strobe is high
--  IPBUS write is high
--  IPBUS readonly is low
--  FIFO full is low
-- Read from the FIFO with the clock of the IPBus when 
--  IPBUS strobe is high
--  IPBUS write is low
--  FIFO empty is low
--  MSB of read shows if FIFO is empty (0) if c_ok_in_msb is high   
--------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use work.ipbus.all;

entity ipbus_fifo is
  generic(
    c_ok_in_msb : std_logic := '0'                 --if high MSB will be 1 if reading ok 
  );
  port(
    c_address  : in std_logic_vector(31 downto 0); --the register's address
    i_clk      : in std_logic;                     --ipbus clock
    i_ipbw     : in ipb_wbus;                      --ipbus write bus
    o_ipbr     : out ipb_rbus;                     --ipbus read bus
    c_readonly : in std_logic;                     --high if register is read only
    i_empty    : in std_logic;                     --empty signal
    i_full     : in std_logic;                     --full signal
    i_rddata   : in std_logic_vector(31 downto 0); --read data
    o_rdclk    : out std_logic;                    --read clock      
    o_rdenable : out std_logic;                    --read enable
    o_wrdata   : out std_logic_vector(31 downto 0);--write data
    o_wrclk    : out std_logic;                    --write clock      
    o_wrenable : out std_logic                     --write enable
  );

end ipbus_fifo;

architecture rtl of ipbus_fifo is
    
    signal m_state   : natural range 0 to 3 := 0;
    signal m_ack     : std_logic := '0';
    signal m_rde     : std_logic := '0';
    signal m_wre     : std_logic := '0';
    signal m_all0    : std_logic_vector(31 downto 0) := (others => '0' );
    signal m_all1    : std_logic_vector(31 downto 0) := (others => '1' );

    signal swap      : std_logic := '0'; --0 for the first word, 1 for the second

begin
    o_ipbr.ipb_addr <= c_address;
    o_ipbr.ipb_ack  <= m_ack;
    o_ipbr.ipb_err  <= '0';

    --assign the read and write clocks and enables of the fifo to the IPBUS clock
    o_rdclk    <= i_clk;
    o_wrclk    <= i_clk;
    o_rdenable <= m_rde;
    o_wrenable <= m_wre;
    
    process(i_clk)
    begin
        if rising_edge(i_clk) then
            if i_ipbw.ipb_strobe='1' and i_ipbw.ipb_write='0' then 
                if m_state=0 and i_empty='0' then
                    --there is something to read in the fifo: start the reading procedure
                    m_ack      <= '0';
                    m_rde      <= '1';
                    m_state    <=  1 ;
                    o_ipbr.ipb_rdata <= m_all1;
                elsif m_state=0 then
                    --there is nothing ro read ... setting default and immediately going to closing
                    o_ipbr.ipb_rdata <= m_all0;
                    m_ack      <= '1';
                    m_state    <=  3 ;            
                elsif m_state=1 then
                    --second stage of good reading: closing the enable and copying the data
                    m_rde      <= '0';
                    m_state    <=  2 ;
                elsif m_state=2 then 
                    if c_ok_in_msb='1' then
                        o_ipbr.ipb_rdata(30 downto 0) <= i_rddata(30 downto 0);
                        o_ipbr.ipb_rdata(31) <= '1';
                        --swap <= not swap;
                    else
                        o_ipbr.ipb_rdata <= i_rddata;
                    end if;
                    m_ack      <= '1';
                    m_state    <=  3 ;
                else                                
                    -- closing the reading
                    m_ack      <= '0';
                    m_state    <=  0 ;
                end if;
            elsif i_ipbw.ipb_strobe='1' and i_ipbw.ipb_write='1' then 
                if m_state=0 and i_full='0' then
                    --we can write to the fifo: start the reading procedure
                    m_ack      <= '0';
                    m_wre      <= '1';
                    m_state    <=  1 ;
                    o_wrdata   <= i_ipbw.ipb_wdata;
                elsif m_state=0 then
                    --cannot write to the fifo
                    m_ack      <= '1';
                    m_state    <=  3 ;
                elsif m_state=1 then
                    --second stage of good reading: closing the enable and copying the data
                    m_wre      <= '0';
                    m_state    <=  2 ;
                elsif m_state=2 then 
                    o_wrdata   <= m_all1;
                    m_ack      <= '1';
                    m_state    <=  3 ;
                else                         
                    -- closing the reading
                    m_ack      <= '0';
                    m_state    <=  0 ;
                end if;
            end if;
        end if;
    end process;    

end rtl;

--architecture fernando of ipbus_fifo is

--    signal o_rdenablei: std_logic;
--    signal ack: std_logic_vector(2 downto 0);
--    signal strobe_d: std_logic;
--    signal reg: std_logic_vector(31 downto 0);

--begin

--    o_ipbr.ipb_addr <= c_address;
--    o_ipbr.ipb_ack <= ack(2);
--    o_ipbr.ipb_err <= '0';
    
--    --assign the read-clock and write-clock of the fifo to the IPBUS clock
--    o_rdclk <= i_clk;
--    o_wrclk <= i_clk;
    
--    process(i_clk)
--    begin
--        if rising_edge(i_clk) then
--            if i_ipbw.ipb_strobe='1' and i_ipbw.ipb_write='1' and c_readonly='0' and i_full='0' then
--                --write
--                o_wrenable <='1';
--                o_wrdata <= i_ipbw.ipb_wdata;
--            elsif i_ipbw.ipb_strobe='1' and strobe_d = '0' and i_ipbw.ipb_write='0' and i_empty='0' then
--                --read
--                o_rdenable <= '1';
--            else
--                o_rdenable <='0';
--                o_wrenable <='0';
--            end if;
--            o_ipbr.ipb_rdata    <= i_rddata;
--            strobe_d            <=  i_ipbw.ipb_strobe;
--            ack(0)              <= i_ipbw.ipb_strobe and not ack(0);
--            for i in 0 to 1 loop
--                ack(i+1)    <= ack(i);
--            end loop;
--        end if;        
--    end process;    

--end fernando;

--architecture carlos of ipbus_fifo is

--    signal o_rdenablei: std_logic;
--    signal ack: std_logic;
--    signal reg: std_logic_vector(31 downto 0);

--begin

--    o_ipbr.ipb_addr <= c_address;
--    o_ipbr.ipb_ack <= ack;
--    o_ipbr.ipb_err <= '0';
    
--    --assign the read-clock and write-clock of the fifo to the IPBUS clock
--    o_rdclk <= i_clk;
--    o_wrclk <= i_clk;
--    o_rdenable <= o_rdenablei;

--    process(i_clk)
--    begin
--        if rising_edge(i_clk) then
--            if i_ipbw.ipb_strobe='1' and i_ipbw.ipb_write='1' and c_readonly='0' and i_full='0' then
--                --write
--                o_wrenable <='1';
--                o_wrdata <= i_ipbw.ipb_wdata;
--            elsif i_ipbw.ipb_strobe='1' and i_ipbw.ipb_write='0' and i_empty='0' then
--                --read
--                o_rdenablei <= '1' and not o_rdenablei;
--                o_ipbr.ipb_rdata <= i_rddata;
--            else
--                o_rdenablei <='0';
--                o_wrenable <='0';
--            end if;
--            ack <= i_ipbw.ipb_strobe and not ack;
--        end if;        
--    end process;    

--end carlos;