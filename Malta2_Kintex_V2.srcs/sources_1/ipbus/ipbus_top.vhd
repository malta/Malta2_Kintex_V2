----------------------------------------------------------------------------------
-- IPBus top
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library unisim;
use unisim.vcomponents.all;
use work.ipbus.all;

entity ipbus_top is
  generic(
    NSLV: positive
   );
  port( 
    -- Control interface
    -------------------------------
    i_sys_rst       : in std_logic;  --System reset
    i_clk200        : in std_logic;  --200MHz clock
    o_clk1hz        : out std_logic; --1Hz output signal
    o_clk_locked    : out std_logic; --High if internal MMCM is ok
    o_eth_locked    : out std_logic; --High if ethernet is latched
    i_addrSwi       : in std_logic_vector(2 downto 0);
    -- IPbus interface 
    -------------------------------
    ipb_to_slaves   : out ipb_wbus_array(NSLV - 1 downto 0);
    ipb_from_slaves : in ipb_rbus_array(NSLV - 1 downto 0);
    o_ipb_clk       : out std_logic;
    o_ipb_tx_led    : out std_logic;
    o_ipb_rx_led    : out std_logic;
    -- Ethernet interface
    -----------------------------
    i_gt_clkp       : in std_logic;  --Gigabit Transceiver CLK+
    i_gt_clkn       : in std_logic;  --Gigabit Transceiver CLK-
    o_gt_txp        : out std_logic; --Gigabit Transceiver TX+s
    o_gt_txn        : out std_logic; --Gigabit Transceiver TX-
    i_gt_rxp        : in std_logic;  --Gigabit Transceiver RX+
    i_gt_rxn        : in std_logic   --Gigabit Transceiver RX- 
  );
  
end ipbus_top;

architecture rtl of ipbus_top is

signal m_ipb_master_w: ipb_wbus;
signal m_ipb_master_r: ipb_rbus;  
signal m_clk125: std_logic;
signal m_clk125_fr: std_logic;
signal m_ipb_clk: std_logic;
signal m_eth_locked: std_logic;
signal m_clk_locked: std_logic;
signal m_rst_125: std_logic;
signal m_rst_eth: std_logic;	
signal m_rst_ipb: std_logic;	
signal m_mac_addr: std_logic_vector(47 downto 0) := (others => '0');
signal m_ip_addr: std_logic_vector(31 downto 0)  := (others => '0');
signal m_pkt_rx: std_logic;
signal m_pkt_tx: std_logic;
signal m_mac_tx_data: std_logic_vector(7 downto 0);
signal m_mac_tx_valid: std_logic;
signal m_mac_tx_last: std_logic;
signal m_mac_tx_error: std_logic;
signal m_mac_tx_ready: std_logic;
signal m_mac_rx_data: std_logic_vector(7 downto 0);
signal m_mac_rx_valid: std_logic;
signal m_mac_rx_last: std_logic;
signal m_mac_rx_error: std_logic;

begin
 
 process(i_addrSwi)
  begin
   case i_addrSwi is 
      when "000" =>
        m_mac_addr <= X"020ddba11599";
        m_ip_addr  <= X"c0a8c801"; -- 192.168.200.1
      when "001" =>
        m_mac_addr <= X"020ddba11600";
        m_ip_addr  <= X"c0a8c80a"; -- 192.168.200.10
      when "010" =>
        m_mac_addr <= X"020ddba11601";
        m_ip_addr  <= X"c0a8c80b"; -- 192.168.200.11
      when "011" =>
        m_mac_addr <= X"020ddba11602";
        m_ip_addr  <= X"c0a8c80c"; -- 192.168.200.12
      when "100" =>
        m_mac_addr <= X"020ddba11603";
        m_ip_addr  <= X"c0a8c80d"; -- 192.168.200.13
      when "101" =>
        m_mac_addr <= X"020ddba11604";
        m_ip_addr  <= X"c0a8c80e"; -- 192.168.200.14
      when "110" =>
        m_mac_addr <= X"020ddba11605";
        m_ip_addr  <= X"c0a8c80f"; -- 192.168.200.15
      when "111" =>
        m_mac_addr <= X"020ddba11606";
        m_ip_addr  <= X"c0a8c810"; -- 192.168.200.16
   end case;
 end process;

    o_ipb_clk <= m_ipb_clk;
    o_clk_locked <= m_clk_locked;
    o_eth_locked <= m_eth_locked;
    
    -- clocks
    clocks: entity work.clocks_7s_serdes
        port map(
            clki_200 => i_clk200,
            clki_fr => m_clk125_fr,
            clki_125 => m_clk125,
            clko_ipb => m_ipb_clk,
            eth_locked => m_eth_locked,
            locked => m_clk_locked,
            nuke => i_sys_rst,
            rsto_125 => m_rst_125,
            rsto_ipb => m_rst_ipb,
            rsto_eth => m_rst_eth,
            onehz => o_clk1hz
        );

    -- ipbus control logic
    control: entity work.ipbus_ctrl
		port map(
			mac_clk => m_clk125,
			rst_macclk => m_rst_125,
			ipb_clk => m_ipb_clk,
			rst_ipb => m_rst_ipb,
			mac_rx_data => m_mac_rx_data,
			mac_rx_valid => m_mac_rx_valid,
			mac_rx_last => m_mac_rx_last,
			mac_rx_error => m_mac_rx_error,
			mac_tx_data => m_mac_tx_data,
			mac_tx_valid => m_mac_tx_valid,
			mac_tx_last => m_mac_tx_last,
			mac_tx_error => m_mac_tx_error,
			mac_tx_ready => m_mac_tx_ready,
			ipb_out => m_ipb_master_w,
			ipb_in => m_ipb_master_r,
			mac_addr => m_mac_addr,
			ip_addr => m_ip_addr,
			pkt_rx => m_pkt_rx,
			pkt_tx => m_pkt_tx,
			pkt_rx_led => o_ipb_rx_led,
			pkt_tx_led => o_ipb_tx_led,
			oob_in => open,
            oob_out => open
		);

    -- IPBUS fabric
    fabric: entity work.ipbus_fabric
      generic map(NSLV => NSLV)
      port map(
        ipb_in => m_ipb_master_w,
        ipb_out => m_ipb_master_r,
        ipb_to_slaves => ipb_to_slaves,
        ipb_from_slaves => ipb_from_slaves
      );


    -- ethernet configuration
	eth: entity work.eth_7s_1000basex
		port map(
			gt_clkp => i_gt_clkp,
			gt_clkn => i_gt_clkn,
			gt_txp => o_gt_txp,
			gt_txn => o_gt_txn,
			gt_rxp => i_gt_rxp,
			gt_rxn => i_gt_rxn,
			clk125_out => m_clk125,
			clk125_fr => m_clk125_fr,
			rsti => m_rst_eth,
			locked => m_eth_locked,
			tx_data => m_mac_tx_data,
			tx_valid => m_mac_tx_valid,
			tx_last => m_mac_tx_last,
			tx_error => m_mac_tx_error,
			tx_ready => m_mac_tx_ready,
			rx_data => m_mac_rx_data,
			rx_valid => m_mac_rx_valid,
			rx_last => m_mac_rx_last,
			rx_error => m_mac_rx_error,
			clk200 => i_clk200
		);

        
end rtl;
