-- The ipbus bus fabric, address select logic, data multiplexers
--
-- The address table is encoded in ipbus_addr_decode package - no need to change
-- anything in this file.
--
-- Dave Newbold, February 2011
-- Carlos Solans, March 2021: Extended to 199 registers

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
use work.ipbus.ALL;

entity ipbus_fabric is
  generic(
    NSLV: positive;
    STROBE_GAP: boolean := false
   );
  port(
    ipb_in: in ipb_wbus;
    ipb_out: out ipb_rbus;
    ipb_to_slaves: out ipb_wbus_array(NSLV - 1 downto 0);
    ipb_from_slaves: in ipb_rbus_array(NSLV - 1 downto 0)
   );

end ipbus_fabric;

architecture rtl of ipbus_fabric is

   signal sel: integer := 0;
   signal ored_ack, ored_err: std_logic_vector(NSLV downto 0);
   signal qstrobe: std_logic;
   signal sel_array: std_logic_vector(199 downto 0);
    
begin

    --carlos's sel
    selector: for i in NSLV-1 downto 0 generate
    begin
        sel_array(i) <= '1' when to_integer(unsigned(ipb_in.ipb_addr)) = to_integer(unsigned(ipb_from_slaves(i).ipb_addr)) else '0';
    end generate;
    
    process(ipb_in.ipb_addr)
	begin
        --default sel
		--sel <= ipbus_addr_sel(ipb_in.ipb_addr);
        --carlos's sel
        if    sel_array(0)='1' then sel <= 0;
        elsif sel_array(1)='1' then sel <= 1;
        elsif sel_array(2)='1' then sel <= 2;
        elsif sel_array(3)='1' then sel <= 3;
        elsif sel_array(4)='1' then sel <= 4;
        elsif sel_array(5)='1' then sel <= 5;
        elsif sel_array(6)='1' then sel <= 6;
        elsif sel_array(7)='1' then sel <= 7;
        elsif sel_array(8)='1' then sel <= 8;
        elsif sel_array(9)='1' then sel <= 9;
        elsif sel_array(10)='1' then sel <= 10;
        elsif sel_array(11)='1' then sel <= 11;
        elsif sel_array(12)='1' then sel <= 12;
        elsif sel_array(13)='1' then sel <= 13;
        elsif sel_array(14)='1' then sel <= 14;
        elsif sel_array(15)='1' then sel <= 15;
        elsif sel_array(16)='1' then sel <= 16;
        elsif sel_array(17)='1' then sel <= 17;
        elsif sel_array(18)='1' then sel <= 18;
        elsif sel_array(19)='1' then sel <= 19;
        elsif sel_array(20)='1' then sel <= 20;
        elsif sel_array(21)='1' then sel <= 21;
        elsif sel_array(22)='1' then sel <= 22;
        elsif sel_array(23)='1' then sel <= 23;
        elsif sel_array(24)='1' then sel <= 24;
        elsif sel_array(25)='1' then sel <= 25;
        elsif sel_array(26)='1' then sel <= 26;
        elsif sel_array(27)='1' then sel <= 27;
        elsif sel_array(28)='1' then sel <= 28;
        elsif sel_array(29)='1' then sel <= 29;
        elsif sel_array(30)='1' then sel <= 30;
        elsif sel_array(31)='1' then sel <= 31;
        elsif sel_array(32)='1' then sel <= 32;
        elsif sel_array(33)='1' then sel <= 33;
        elsif sel_array(34)='1' then sel <= 34;
        elsif sel_array(35)='1' then sel <= 35;
        elsif sel_array(36)='1' then sel <= 36;
        elsif sel_array(37)='1' then sel <= 37;
        elsif sel_array(38)='1' then sel <= 38;
        elsif sel_array(39)='1' then sel <= 39;
        elsif sel_array(40)='1' then sel <= 40;
        elsif sel_array(41)='1' then sel <= 41;
        elsif sel_array(42)='1' then sel <= 42;
        elsif sel_array(43)='1' then sel <= 43;
        elsif sel_array(44)='1' then sel <= 44;
        elsif sel_array(45)='1' then sel <= 45;
        elsif sel_array(46)='1' then sel <= 46;
        elsif sel_array(47)='1' then sel <= 47;
        elsif sel_array(48)='1' then sel <= 48;
        elsif sel_array(49)='1' then sel <= 49;
        elsif sel_array(50)='1' then sel <= 50;
        elsif sel_array(51)='1' then sel <= 51;
        elsif sel_array(52)='1' then sel <= 52;
        elsif sel_array(53)='1' then sel <= 53;
        elsif sel_array(54)='1' then sel <= 54;
        elsif sel_array(55)='1' then sel <= 55;
        elsif sel_array(56)='1' then sel <= 56;
        elsif sel_array(57)='1' then sel <= 57;
        elsif sel_array(58)='1' then sel <= 58;
        elsif sel_array(59)='1' then sel <= 59;
        elsif sel_array(60)='1' then sel <= 60;
        elsif sel_array(61)='1' then sel <= 61;
        elsif sel_array(62)='1' then sel <= 62;
        elsif sel_array(63)='1' then sel <= 63;
        elsif sel_array(64)='1' then sel <= 64;
        elsif sel_array(65)='1' then sel <= 65;
        elsif sel_array(66)='1' then sel <= 66;
        elsif sel_array(67)='1' then sel <= 67;
        elsif sel_array(68)='1' then sel <= 68;
        elsif sel_array(69)='1' then sel <= 69;
        elsif sel_array(70)='1' then sel <= 70;
        elsif sel_array(71)='1' then sel <= 71;
        elsif sel_array(72)='1' then sel <= 72;
        elsif sel_array(73)='1' then sel <= 73;
        elsif sel_array(74)='1' then sel <= 74;
        elsif sel_array(75)='1' then sel <= 75;
        elsif sel_array(76)='1' then sel <= 76;
        elsif sel_array(77)='1' then sel <= 77;
        elsif sel_array(78)='1' then sel <= 78;
        elsif sel_array(79)='1' then sel <= 79;
        elsif sel_array(80)='1' then sel <= 80;
        elsif sel_array(81)='1' then sel <= 81;
        elsif sel_array(82)='1' then sel <= 82;
        elsif sel_array(83)='1' then sel <= 83;
        elsif sel_array(84)='1' then sel <= 84;
        elsif sel_array(85)='1' then sel <= 85;
        elsif sel_array(86)='1' then sel <= 86;
        elsif sel_array(87)='1' then sel <= 87;
        elsif sel_array(88)='1' then sel <= 88;
        elsif sel_array(89)='1' then sel <= 89;
        elsif sel_array(90)='1' then sel <= 90;
        elsif sel_array(91)='1' then sel <= 91;
        elsif sel_array(92)='1' then sel <= 92;
        elsif sel_array(93)='1' then sel <= 93;
        elsif sel_array(94)='1' then sel <= 94;
        elsif sel_array(95)='1' then sel <= 95;
        elsif sel_array(96)='1' then sel <= 96;
        elsif sel_array(97)='1' then sel <= 97;
        elsif sel_array(98)='1' then sel <= 98;
        elsif sel_array(99)='1' then sel <= 99;
        elsif sel_array(100)='1' then sel <= 100;
        elsif sel_array(101)='1' then sel <= 101;
        elsif sel_array(102)='1' then sel <= 102;
        elsif sel_array(103)='1' then sel <= 103;
        elsif sel_array(104)='1' then sel <= 104;
        elsif sel_array(105)='1' then sel <= 105;
        elsif sel_array(106)='1' then sel <= 106;
        elsif sel_array(107)='1' then sel <= 107;
        elsif sel_array(108)='1' then sel <= 108;
        elsif sel_array(109)='1' then sel <= 109;
        elsif sel_array(110)='1' then sel <= 110;
        elsif sel_array(111)='1' then sel <= 111;
        elsif sel_array(112)='1' then sel <= 112;
        elsif sel_array(113)='1' then sel <= 113;
        elsif sel_array(114)='1' then sel <= 114;
        elsif sel_array(115)='1' then sel <= 115;
        elsif sel_array(116)='1' then sel <= 116;
        elsif sel_array(117)='1' then sel <= 117;
        elsif sel_array(118)='1' then sel <= 118;
        elsif sel_array(119)='1' then sel <= 119;
        elsif sel_array(120)='1' then sel <= 120;
        elsif sel_array(121)='1' then sel <= 121;
        elsif sel_array(122)='1' then sel <= 122;
        elsif sel_array(123)='1' then sel <= 123;
        elsif sel_array(124)='1' then sel <= 124;
        elsif sel_array(125)='1' then sel <= 125;
        elsif sel_array(126)='1' then sel <= 126;
        elsif sel_array(127)='1' then sel <= 127;
        elsif sel_array(128)='1' then sel <= 128;
        elsif sel_array(129)='1' then sel <= 129;
        elsif sel_array(130)='1' then sel <= 130;
        elsif sel_array(131)='1' then sel <= 131;
        elsif sel_array(132)='1' then sel <= 132;
        elsif sel_array(133)='1' then sel <= 133;
        elsif sel_array(134)='1' then sel <= 134;
        elsif sel_array(135)='1' then sel <= 135;
        elsif sel_array(136)='1' then sel <= 136;
        elsif sel_array(137)='1' then sel <= 137;
        elsif sel_array(138)='1' then sel <= 138;
        elsif sel_array(139)='1' then sel <= 139;
        elsif sel_array(140)='1' then sel <= 140;
        elsif sel_array(141)='1' then sel <= 141;
        elsif sel_array(142)='1' then sel <= 142;
        elsif sel_array(143)='1' then sel <= 143;
        elsif sel_array(144)='1' then sel <= 144;
        elsif sel_array(145)='1' then sel <= 145;
        elsif sel_array(146)='1' then sel <= 146;
        elsif sel_array(147)='1' then sel <= 147;
        elsif sel_array(148)='1' then sel <= 148;
        elsif sel_array(149)='1' then sel <= 149;
        elsif sel_array(150)='1' then sel <= 150;
        elsif sel_array(151)='1' then sel <= 151;
        elsif sel_array(152)='1' then sel <= 152;
        elsif sel_array(153)='1' then sel <= 153;
        elsif sel_array(154)='1' then sel <= 154;
        elsif sel_array(155)='1' then sel <= 155;
        elsif sel_array(156)='1' then sel <= 156;
        elsif sel_array(157)='1' then sel <= 157;
        elsif sel_array(158)='1' then sel <= 158;
        elsif sel_array(159)='1' then sel <= 159;
        elsif sel_array(160)='1' then sel <= 160;
        elsif sel_array(161)='1' then sel <= 161;
        elsif sel_array(162)='1' then sel <= 162;
        elsif sel_array(163)='1' then sel <= 163;
        elsif sel_array(164)='1' then sel <= 164;
        elsif sel_array(165)='1' then sel <= 165;
        elsif sel_array(166)='1' then sel <= 166;
        elsif sel_array(167)='1' then sel <= 167;
        elsif sel_array(168)='1' then sel <= 168;
        elsif sel_array(169)='1' then sel <= 169;
        elsif sel_array(170)='1' then sel <= 170;
        elsif sel_array(171)='1' then sel <= 171;
        elsif sel_array(172)='1' then sel <= 172;
        elsif sel_array(173)='1' then sel <= 173;
        elsif sel_array(174)='1' then sel <= 174;
        elsif sel_array(175)='1' then sel <= 175;
        elsif sel_array(176)='1' then sel <= 176;
        elsif sel_array(177)='1' then sel <= 177;
        elsif sel_array(178)='1' then sel <= 178;
        elsif sel_array(179)='1' then sel <= 179;
        elsif sel_array(180)='1' then sel <= 180;
        elsif sel_array(181)='1' then sel <= 181;
        elsif sel_array(182)='1' then sel <= 182;
        elsif sel_array(183)='1' then sel <= 183;
        elsif sel_array(184)='1' then sel <= 184;
        elsif sel_array(185)='1' then sel <= 185;
        elsif sel_array(186)='1' then sel <= 186;
        elsif sel_array(187)='1' then sel <= 187;
        elsif sel_array(188)='1' then sel <= 188;
        elsif sel_array(189)='1' then sel <= 189;
        elsif sel_array(190)='1' then sel <= 190;
        elsif sel_array(191)='1' then sel <= 191;
        elsif sel_array(192)='1' then sel <= 192;
        elsif sel_array(193)='1' then sel <= 193;
        elsif sel_array(194)='1' then sel <= 194;
        elsif sel_array(195)='1' then sel <= 195;
        elsif sel_array(196)='1' then sel <= 196;
        elsif sel_array(197)='1' then sel <= 197;
        elsif sel_array(198)='1' then sel <= 198;
        else 
            sel <= 199;
        end if;
    end process;

    ored_ack(NSLV) <= '0';
    ored_err(NSLV) <= '0';
	
    qstrobe <= ipb_in.ipb_strobe when STROBE_GAP = false else
               ipb_in.ipb_strobe and not (ored_ack(0) or ored_err(0));

    busgen: for i in NSLV-1 downto 0 generate
    begin

      ipb_to_slaves(i).ipb_addr <= ipb_in.ipb_addr;
      ipb_to_slaves(i).ipb_wdata <= ipb_in.ipb_wdata;
      ipb_to_slaves(i).ipb_strobe <= qstrobe when sel=i else '0';
      ipb_to_slaves(i).ipb_write <= ipb_in.ipb_write;
      ored_ack(i) <= ored_ack(i+1) or ipb_from_slaves(i).ipb_ack;
      ored_err(i) <= ored_err(i+1) or ipb_from_slaves(i).ipb_err;		

    end generate;

    ipb_out.ipb_rdata <= ipb_from_slaves(sel).ipb_rdata when sel /= 99 else (others => '0');
    ipb_out.ipb_ack <= ored_ack(0);
    ipb_out.ipb_err <= ored_err(0);
    ipb_out.ipb_addr <= (others => '0');
  
end rtl;

