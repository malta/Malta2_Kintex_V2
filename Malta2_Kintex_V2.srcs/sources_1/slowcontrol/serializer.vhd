library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.package_constants.all;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

entity serializer is
port(
    i_clock        : in std_logic;
    i_dataParallel : in std_logic_vector(4321 downto 0);
    o_dataSerial   : out std_logic;    
    i_startW       : in std_logic;   --VD: it hsould be user responsability to keep it on for just one clk cycle
    --i_RW_switch    : in std_logic := '0'; --VD: I can easily read/write in paralle, and in ay case this should be user's responsability rom outside
    --o_busy         : out std_logic;       --VD: busy is not needed since enable does the same thing
    --o_enable       : out std_logic;
    o_ser_load      : out std_logic
);
end serializer;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

architecture ser of serializer is

    --------
    -- wires
    --------
    
    signal m_i_clock        : std_logic := '0';
    signal m_i_dataParallel : std_logic_vector(4321 downto 0) := (others => '0');
    signal m_o_dataSerial   : std_logic := '1';
    --signal m_o_busy         : std_logic := '0';
    --signal m_enable         : std_logic := '0';
    signal m_load_ser       : std_logic := '0';
    -------------------------
    -- counter: 16+1 bit word
    -------------------------
    
    signal m_counter_bitsInWord : natural range 0 to 4323 := 0; -- cycle 0 -> send 0, cycle NBITS+1 -> reset --481

begin

    ----------
    -- process
    ----------
    --todo: cache the word before transmission

    process_serializer : process (m_i_clock) is
    begin
        if rising_edge(m_i_clock) then
            
            if i_startW='1' and m_counter_bitsInWord=0 then
                --m_enable <= '1';
                --m_o_busy <= '1';
                m_i_dataParallel <= i_dataParallel;
                m_o_dataSerial   <= i_dataParallel(m_counter_bitsInWord);
                m_counter_bitsInWord <= 1;
                m_load_ser <= '0'; 
            elsif m_counter_bitsInWord=0 then
                --m_enable <= '0';
                m_o_dataSerial <= '0';
                m_load_ser <= '0'; 
            else
                if m_counter_bitsInWord=4321 then
                  m_load_ser <= '1'; 
                  m_counter_bitsInWord <= m_counter_bitsInWord+1;
                  m_o_dataSerial       <= m_i_dataParallel(m_counter_bitsInWord);  --Ignatio's fix for Width              
                elsif m_counter_bitsInWord=4322 then --480
                  --m_enable <= '0';
                  --m_o_busy <= '0';
                  m_o_dataSerial <= '0';
                  m_counter_bitsInWord<= 0;
                  m_load_ser <= '0'; 
                else
                  m_o_dataSerial       <= m_i_dataParallel(m_counter_bitsInWord);
                  m_counter_bitsInWord <= m_counter_bitsInWord+1;
                  m_load_ser <= '0';
                end if;
            end if;
        end if;        
            
    end process process_serializer;
    m_i_clock        <= i_clock;
    o_dataSerial <= m_o_dataSerial;
    --o_enable     <= m_enable;  
    o_ser_load  <= m_load_ser; 

end ser;
