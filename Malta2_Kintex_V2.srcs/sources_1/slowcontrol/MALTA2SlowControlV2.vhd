----------------------------------------------------------------------------------
-- MALTA2 Slow Control
-- Leyre Flores
-- Ignacio Asensi
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
library UNISIM;
use UNISIM.VComponents.all;
library unimacro;
use unimacro.vcomponents.all;
use work.ipbus.all;
use work.readout_constants.all;
use work.package_constants.all;

entity MALTA2SlowControlV2 is
  Port ( 
    signal o_SC_load        : out    std_logic; 
    signal o_reading        : out    std_logic;-- If '1' it means that I am reading
    signal o_SCclk_enable   : out    std_logic;
    signal o_FROclk_enable  : out    std_logic;   
    signal o_SROclk_enable  : out    std_logic;
    signal o_SLOW_enable    : out    std_logic;
    signal o_IPB_L1A        : out    std_logic;
    signal i_clk_sc         : in     std_logic;-- Connect a 40MHz clock
    signal i_sout           : in     std_logic;-- Values of all registers as a serial stream from MALTA2
    signal o_rstB           : out    std_logic;-- If '0' no input data is taken by MALTA2 and the config of the chip is set to default
    signal o_data_toMalta   : out    std_logic; 
    -- control
    signal i_IPBeset         : in   std_logic;
    -- ipbus signals
    signal i_clk_ipb        : in     std_logic;-- Connect a 32MHz clock
    signal i_ipbw           : in     ipb_wbus_array(199 downto 0);
    signal i_ipbr           : inout  ipb_rbus_array(199 downto 0);
    signal o_LPC_LEDS       : out std_logic_vector(9 downto 0);
    signal o_LPC_SMA       : out std_logic_vector(9 downto 0)
  );
end MALTA2SlowControlV2;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
architecture Behavioral of MALTA2SlowControlV2 is
-- MALTA2---------
signal  DATAFLOW_MERGERTOLEFT           : std_logic := '1';
signal  DATAFLOW_MERGERTORIGHT          : std_logic := '0';
signal  DATAFLOW_LMERGERTOLVDS          : std_logic := '1';
signal  DATAFLOW_LMERGERTOCMOS          : std_logic := '1';
signal  DATAFLOW_RMERGERTOLVDS          : std_logic := '0';
signal  DATAFLOW_RMERGERTOCMOS          : std_logic := '0';
signal  DATAFLOW_LCMOS                  : std_logic := '0';
signal  DATAFLOW_RCMOS                  : std_logic := '0';
signal  DATAFLOW_ENLVDS                 : std_logic := '1';
signal  DATAFLOW_ENLCMOS                : std_logic := '1';
signal  DATAFLOW_ENRCMOS                : std_logic := '0';
signal  DATAFLOW_ENMERGER               : std_logic := '0';
signal  POWER_SWITCH_LEFT               : std_logic := '1';
signal  POWER_SWITCH_RIGHT              : std_logic := '0';
signal  LVDS_EN                         : std_logic := '1';
signal  LVDS_PRE                        : std_logic_vector(15 downTo 0):= (others => '0');--X"C001";
signal  LVDS_BRIDGE_EN                  : std_logic_vector( 4 downTo 0):= (others => '0');--X"1C";
signal  LVDS_CMFB_EN                    : std_logic_vector( 4 downTo 0):= (others => '0');--X"1C";
signal  LVDS_SET_IBCMFB                 : std_logic_vector( 3 downTo 0):= (others => '0');--X"7";
signal  LVDS_SET_IVPH                   : std_logic_vector( 3 downTo 0):= (others => '0');--X"2";
signal  LVDS_SET_IVPL                   : std_logic_vector( 3 downTo 0):= (others => '0');--X"D";
signal  LVDS_SET_IVNH                   : std_logic_vector( 3 downTo 0):= (others => '0');--X"D";
signal  LVDS_SET_IVNL                   : std_logic_vector( 3 downTo 0):= (others => '0');--X"7";
signal  SWCNTL_VCASN                    : std_logic:= '0';
signal  SWCNTL_VCLIP                    : std_logic := '0';
signal  SWCNTL_VPLSE_HIGH               : std_logic := '0';
signal  SWCNTL_VPLSE_LOW                : std_logic := '0';
signal  SWCNTL_VRESET_P                 : std_logic := '0';
signal  SWCNTL_VRESET_D                 : std_logic := '0';
signal  SWCNTL_ICASN                    : std_logic := '0';
signal  SWCNTL_IRESET                   : std_logic := '0';
signal  SWCNTL_IBIAS                    : std_logic := '0';
signal  SWCNTL_ITHR                     : std_logic := '0';
signal  SWCNTL_IDB                      : std_logic := '0';
signal  SWCNTL_IREF                     : std_logic := '0';
signal  SET_IRESET_BIT                  : std_logic := '1';
signal  SWCNTL_DACMONV                  : std_logic := '0';
signal  SWCNTL_DACMONI                  : std_logic := '0';
signal  SET_IBUFP_MON_0                 : std_logic_vector(3 downTo 0) := (others => '0');--X"5";--(others => '1');
signal  SET_IBUFN_MON_0                 : std_logic_vector(3 downTo 0) := (others => '0');--X"9";--(others => '1');
signal  SET_IBUFP_MON_1                 : std_logic_vector(3 downTo 0) := (others => '0');--X"5";--(others => '1');
signal  SET_IBUFN_MON_1                 : std_logic_vector(3 downTo 0) := (others => '0');--X"9";--(others => '1');
signal  PULSE_MON_L                     : std_logic := '0';
signal  PULSE_MON_R                     : std_logic := '0';
signal  LVDS_VBCMFB                     : std_logic_vector(3 downTo 0):= (others => '0');--"1110";--(others => '1');
-- nacho
signal 	SET_VCASN                       : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_VCLIP                       : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_VPLSE_HIGH                  : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_VPLSE_LOW                   : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_VRESET_P                    : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_VRESET_D                    : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_ICASN                       : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_IRESET                      : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_ITHR                        : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_IBIAS                       : std_logic_vector(127 downTo 0) := (others => '0');
signal 	SET_IDB                         : std_logic_vector(127 downTo 0) := (others => '0');
signal 	MASK_COL                        : std_logic_vector(511 downTo 0) := (others => '0');
signal 	MASK_HOR                        : std_logic_vector(511 downTo 0) := (others => '0');
signal 	MASK_DIAG                       : std_logic_vector(511 downTo 0) := (others => '0');
signal 	MASK_FULLCOL                    : std_logic_vector(255 downTo 0) := (others => '0');
signal 	PULSE_COL                       : std_logic_vector(511 downTo 0) := (others => '0');
signal 	PULSE_HOR                       : std_logic_vector(511 downTo 0) := (others => '0');



signal  R_DATAFLOW_MERGERTOLEFT           : std_logic ;
signal  R_DATAFLOW_MERGERTORIGHT          : std_logic ;
signal  R_DATAFLOW_LMERGERTOLVDS          : std_logic ;
signal  R_DATAFLOW_LMERGERTOCMOS          : std_logic ;
signal  R_DATAFLOW_RMERGERTOLVDS          : std_logic ;
signal  R_DATAFLOW_RMERGERTOCMOS          : std_logic ;
signal  R_DATAFLOW_LCMOS                  : std_logic ;
signal  R_DATAFLOW_RCMOS                  : std_logic ;
signal  R_DATAFLOW_ENLVDS                 : std_logic ;
signal  R_DATAFLOW_ENLCMOS                : std_logic ;
signal  R_DATAFLOW_ENRCMOS                : std_logic ;
signal  R_DATAFLOW_ENMERGER               : std_logic ;
signal  R_POWER_SWITCH_LEFT               : std_logic ;
signal  R_POWER_SWITCH_RIGHT              : std_logic ;
signal  R_LVDS_EN                         : std_logic ;
signal  R_LVDS_PRE                        : std_logic_vector(15 downTo 0);
signal  R_LVDS_BRIDGE_EN                  : std_logic_vector( 4 downTo 0) ;
signal  R_LVDS_CMFB_EN                    : std_logic_vector( 4 downTo 0);
signal  R_LVDS_SET_IBCMFB                 : std_logic_vector( 3 downTo 0);
signal  R_LVDS_SET_IVPH                   : std_logic_vector( 3 downTo 0);
signal  R_LVDS_SET_IVPL                   : std_logic_vector( 3 downTo 0);
signal  R_LVDS_SET_IVNH                   : std_logic_vector( 3 downTo 0);
signal  R_LVDS_SET_IVNL                   : std_logic_vector( 3 downTo 0);
signal  R_SWCNTL_VCASN                    : std_logic;
signal  R_SWCNTL_VCLIP                    : std_logic ;
signal  R_SWCNTL_VPLSE_HIGH               : std_logic ;
signal  R_SWCNTL_VPLSE_LOW                : std_logic ;
signal  R_SWCNTL_VRESET_P                 : std_logic ;
signal  R_SWCNTL_VRESET_D                 : std_logic ;
signal  R_SWCNTL_ICASN                    : std_logic ;
signal  R_SWCNTL_IRESET                   : std_logic ;
signal  R_SWCNTL_IBIAS                    : std_logic ;
signal  R_SWCNTL_ITHR                     : std_logic ;
signal  R_SWCNTL_IDB                      : std_logic ;
signal  R_SWCNTL_IREF                     : std_logic ;
signal  R_SET_IRESET_BIT                  : std_logic ;
signal  R_SWCNTL_DACMONV                  : std_logic ;
signal  R_SWCNTL_DACMONI                  : std_logic ;
signal  R_SET_IBUFP_MON_0                 : std_logic_vector(3 downTo 0) ;
signal  R_SET_IBUFN_MON_0                 : std_logic_vector(3 downTo 0) ;
signal  R_SET_IBUFP_MON_1                 : std_logic_vector(3 downTo 0) ;
signal  R_SET_IBUFN_MON_1                 : std_logic_vector(3 downTo 0) ;
signal  R_PULSE_MON_L                     : std_logic ;
signal  R_PULSE_MON_R                     : std_logic ;
signal  R_LVDS_VBCMFB                     : std_logic_vector(3 downTo 0);

--New simple signals added by Leyre
signal m_SC_SMALL_1_W                   : std_logic_vector(31 downto 0) := (others => '0');
signal m_LVDS_BRIDGE_CMF_IB_IV          : std_logic_vector(31 downto 0) := (others => '0');
signal m_SWCNTL_IBUFMON                 : std_logic_vector(31 downto 0) := (others => '0');
signal m_PULSE_MON_DELAY                : std_logic_vector(31 downto 0) := (others => '0');

--New signals to read the simple IPBUs registers
signal m_SC_SMALL_1_R                   : std_logic_vector(31 downto 0) := (others => '0');
signal m_LVDS_BRIDGE_CMF_IB_IV_R        : std_logic_vector(31 downto 0) := (others => '0');
signal m_SWCNTL_IBUFMON_R               : std_logic_vector(31 downto 0) := (others => '0');
signal m_PULSE_MON_DELAY_R              : std_logic_vector(31 downto 0) := (others => '0');
signal m_split                          : std_logic_vector(31 downto 0) := (others => '0');
signal  m_control                       : std_logic_vector(31 downto 0) := (others => '0');



signal m_SC_load                        : std_logic := '0'; -- control of sending the slowcontrol to chip
signal m_sp_word                        : std_logic := '0'; 
signal m_sp_pos                         : std_logic := '0'; 
signal m_sp_data                        : std_logic := '0'; 

--Changed was signal m_WWORD_full : std_logic_vector(4321 downto 0) := (others => '0');
signal m_WWORD_full             : std_logic_vector(4223 downto 0) := (others => '0');
signal m_fifoW_wren             : std_logic := '0';
signal m_fifoW_rden             : std_logic := '0';
signal m_fifoW_WWORD_reg        : std_logic_vector(31 downto 0) := (others => '0');
signal m_fifoW_rdclk            : std_logic := '0';
signal m_fifoW_WWORD_dummy      : std_logic_vector(31 downto 0) := (others => '0');
signal m_fifoW_empty            : std_logic := '0';
signal m_fifoW_full             : std_logic := '1';
signal m_fifoW_almostFull       : std_logic := '1';


-- READ WORD TO IPBUS
signal m_fifoR_reset            : std_logic := '0';
signal m_RWORD                  : std_logic_vector(255 downto 0) := (others => '0');
signal m_RWORD_full             : std_logic_vector(4223 downto 0) := (others => '0');
signal m_fifoR_wren             : std_logic := '0';
signal m_fifoR_rden             : std_logic := '0';
signal m_fifoR_RWORD_reg        : std_logic_vector(31 downto 0) := (others => '0');
signal m_fifoR_rdclk            : std_logic := '0';

signal m_fifoR_empty       : std_logic := '0';
signal m_fifoR_full        : std_logic := '1';
signal m_fifoR_almostFull  : std_logic := '1';


------- Define m_ signals for sc ports
signal m_serializer_toMalta             : std_logic := '0';
signal m_serializer_fromMalta           : std_logic := '0';
signal m_sout_ack                       : std_logic := '0';

signal m_start                          : std_logic := '0';
signal m_WR_start                       : std_logic := '0';
signal m_RE_start                       : std_logic := '0';
signal m_ReadEn                         : std_logic :='0';
signal m_ReadRST                        : std_logic :='0';
signal m_WriteEn                        : std_logic :='0';
signal m_WriteRST                       : std_logic :='1';
signal m_SC_state                       : std_logic_vector(2 downto 0):="000";
signal m_SC_stateL                      : std_logic_vector(2 downto 0):="000";
signal m_des_en                         : std_logic :='0';
signal m_serializer_start               : std_logic :='0';
signal m_des_busy                       : std_logic :='0';
--- MALTA WORD
signal m_SC_data_toMalta                : std_logic_vector(4321 downto 0) := (others => '0');
signal m_SC_data_toUser                 : std_logic_vector(4321 downto 0) := (others => '0');

signal im_writing                       : std_logic :='0';
signal im_reading                       : std_logic :='0';

--counters
signal m_writing_counter                : natural range 0 to 8790 := 0;
signal m_writing_fifo_counter           : integer range 0 to 257 := 0;
signal m_R_start                        : std_logic := '0';
signal m_write_to_ipbus_done            : std_logic := '0';

--Abhi's LED and SMA board for debugging
signal m_LPC_LEDS_SC                : std_logic_vector(9 downto 0) := (others => '0');
signal m_i_sout_to_SMA_direct       : std_logic;
signal m_i_sout_to_SMA_inclkprocess : std_logic; 



function reverse_vectorV (a : in std_logic_vector)
    return std_logic_vector is
    variable result : std_logic_vector(a'range);
    alias aa        : std_logic_vector(a'reverse_range) is a;
    begin
        for i in aa'range loop
            result(i) := aa(i);
        end loop;
        return result;
    end;  -- function reverse_any_vector    
 --read
component fifo_SC_toUser256 is
    port (
        rst           : IN STD_LOGIC;
        wr_clk        : IN STD_LOGIC;
        rd_clk        : IN STD_LOGIC;
        din           : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
        wr_en         : IN STD_LOGIC;
        rd_en         : IN STD_LOGIC;
        dout          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
        full          : OUT STD_LOGIC;
        almost_full   : OUT STD_LOGIC;
        empty         : OUT STD_LOGIC
    );
end component fifo_SC_toUser256; 
--write 
component fifo_user_toSC256 is
    port (
        rst           : IN STD_LOGIC;
        wr_clk        : IN STD_LOGIC;
        rd_clk        : IN STD_LOGIC;
        din           : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
        wr_en         : IN STD_LOGIC;
        rd_en         : IN STD_LOGIC;
        dout          : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
        full          : OUT STD_LOGIC;
        almost_full   : OUT STD_LOGIC;
        empty         : OUT STD_LOGIC
    );
end component fifo_user_toSC256; 
begin

 -- SERIALIZER
 portMap_serializer : entity work.serializer
 port map(
   i_clock        => i_clk_sc,
   i_dataParallel => m_SC_data_toMalta,
   i_startW       => m_WR_start,   
   o_dataSerial   => m_serializer_toMalta,
   o_enable       => m_serializer_start, -- it is high when sending serialized data
   --o_busy         => open
   o_ser_load     => m_SC_load 
 );
  
 portMap_deserializer: entity work.deserializer
 port map(
   i_clock        => i_clk_sc ,
   i_dataSerial   => i_sout,--m_serializer_toMalta,--i_sout,
--   o_busy         => m_des_busy, -- high when no active and when done
   o_dataParallel => m_SC_data_toUser,
   i_startR       => m_R_start  
 );


o_LPC_SMA(0)   <=i_sout;
o_LPC_SMA(1)   <=m_des_busy;--m_i_sout_to_SMA_inclkprocess;
o_LPC_SMA(2)   <=m_R_start; 
o_LPC_SMA(3)   <=im_reading;-- im_writing;
o_LPC_SMA(4)   <=m_serializer_toMalta;
o_LPC_SMA(5)   <=m_serializer_start; 
o_LPC_SMA(6)   <=m_WR_start;
o_LPC_SMA(7)   <=im_writing;--m_fifoR_wren;
o_LPC_SMA(9)   <=i_clk_sc; 

o_SC_load       <= m_SC_load; 
o_data_toMalta <= m_serializer_toMalta;
o_rstB         <= m_serializer_start;



m_SC_data_toMalta(0)                    <= DATAFLOW_MERGERTOLEFT;
m_SC_data_toMalta(1)                    <= DATAFLOW_MERGERTORIGHT;
m_SC_data_toMalta(2)                    <= DATAFLOW_LMERGERTOLVDS;
m_SC_data_toMalta(3)                    <= DATAFLOW_LMERGERTOCMOS;
m_SC_data_toMalta(4)                    <= DATAFLOW_RMERGERTOLVDS;
m_SC_data_toMalta(5)                    <= DATAFLOW_RMERGERTOCMOS;
m_SC_data_toMalta(6)                    <= DATAFLOW_LCMOS;
m_SC_data_toMalta(7)                    <= DATAFLOW_RCMOS;
m_SC_data_toMalta(8)                    <= DATAFLOW_ENLVDS;
m_SC_data_toMalta(9)                    <= DATAFLOW_ENLCMOS;
m_SC_data_toMalta(10)                   <= DATAFLOW_ENRCMOS;
m_SC_data_toMalta(11)                   <= DATAFLOW_ENMERGER;
m_SC_data_toMalta(12)                   <= POWER_SWITCH_LEFT;
m_SC_data_toMalta(13)                   <= POWER_SWITCH_RIGHT;
m_SC_data_toMalta(14)                   <= LVDS_EN;
m_SC_data_toMalta(30 downTo 15)         <= LVDS_PRE;
m_SC_data_toMalta(35 downTo 31)         <= LVDS_BRIDGE_EN;
m_SC_data_toMalta(40 downTo 36)         <= LVDS_CMFB_EN;
m_SC_data_toMalta(44 downTo 41)         <= LVDS_SET_IBCMFB;
m_SC_data_toMalta(48 downTo 45)         <= LVDS_SET_IVPH;
m_SC_data_toMalta(52 downTo 49)         <= LVDS_SET_IVPL;
m_SC_data_toMalta(56 downTo 53)         <= LVDS_SET_IVNH;
m_SC_data_toMalta(60 downTo 57)         <= LVDS_SET_IVNL;
m_SC_data_toMalta(61)                   <= SWCNTL_VCASN;
m_SC_data_toMalta(62)                   <= SWCNTL_VCLIP;
m_SC_data_toMalta(63)                   <= SWCNTL_VPLSE_HIGH;
m_SC_data_toMalta(64)                   <= SWCNTL_VPLSE_LOW;
m_SC_data_toMalta(65)                   <= SWCNTL_VRESET_P;
m_SC_data_toMalta(66)                   <= SWCNTL_VRESET_D;
m_SC_data_toMalta(67)                   <= SWCNTL_ICASN;
m_SC_data_toMalta(68)                   <= SWCNTL_IRESET;
m_SC_data_toMalta(69)                   <= SWCNTL_IBIAS;
m_SC_data_toMalta(70)                   <= SWCNTL_ITHR;
m_SC_data_toMalta(71)                   <= SWCNTL_IDB;
m_SC_data_toMalta(72)                   <= SWCNTL_IREF;
m_SC_data_toMalta(73)                   <= SET_IRESET_BIT;
m_SC_data_toMalta(74)                   <= SWCNTL_DACMONV;
m_SC_data_toMalta(75)                   <= SWCNTL_DACMONI;
m_SC_data_toMalta(79 downTo 76)         <= SET_IBUFP_MON_0;
m_SC_data_toMalta(83 downTo 80)         <= SET_IBUFN_MON_0;
m_SC_data_toMalta(87 downTo 84)         <= SET_IBUFP_MON_1;
m_SC_data_toMalta(91 downTo 88)         <= SET_IBUFN_MON_1;
m_SC_data_toMalta(219 downTo 92)        <= SET_VCASN;
m_SC_data_toMalta(347 downTo 220)       <= SET_VCLIP;
m_SC_data_toMalta(475 downTo 348)       <= SET_VPLSE_HIGH;
m_SC_data_toMalta(603 downTo 476)       <= SET_VPLSE_LOW;
m_SC_data_toMalta(731 downTo 604)       <= SET_VRESET_P;
m_SC_data_toMalta(859 downTo 732)       <= SET_VRESET_D;
m_SC_data_toMalta(987 downTo 860)       <= SET_ICASN;
m_SC_data_toMalta(1115 downTo 988)      <= SET_IRESET;
m_SC_data_toMalta(1243 downTo 1116)     <= SET_ITHR;
m_SC_data_toMalta(1371 downTo 1244)     <= SET_IBIAS;
m_SC_data_toMalta(1499 downTo 1372)     <= SET_IDB;
m_SC_data_toMalta(2011 downTo 1500)     <= MASK_COL;
m_SC_data_toMalta(2523 downTo 2012)     <= MASK_HOR;
m_SC_data_toMalta(3035 downTo 2524)     <= MASK_DIAG;
m_SC_data_toMalta(3291 downTo 3036)     <= MASK_FULLCOL;
m_SC_data_toMalta(3803 downTo 3292)     <= PULSE_COL;
m_SC_data_toMalta(4315 downTo 3804)     <= PULSE_HOR;
m_SC_data_toMalta(4316)                 <= PULSE_MON_L;
m_SC_data_toMalta(4317)                 <= PULSE_MON_R;
m_SC_data_toMalta(4321 downTo 4318)     <= LVDS_VBCMFB;
 ------------------------------------------------------------------------------------ 
 process(i_clk_sc) is
  begin    
  if rising_edge(i_clk_sc) then
    m_SC_stateL <= m_SC_state;
  end if;
 end process;  
    
 process(i_clk_sc) is
  begin
  if rising_edge(i_clk_sc) then
    m_i_sout_to_SMA_inclkprocess <= i_sout; 
    if (m_SC_stateL = "000") then  -- resetting
        m_LPC_LEDS_SC       <= "1000000001";
        m_writing_counter   <= 0;
        m_writing_fifo_counter <=0;
        m_WR_start          <='0';
        m_fifoR_reset       <='1';
        m_write_to_ipbus_done<='0';
        m_RWORD_full <= (others => '0');
        m_WWORD_full <= (others => '1');

    elsif (m_SC_stateL = "001") then                                                -- STATE SET FROM SW
        if (m_writing_counter< 0 + CC_WRITE_FIFO ) then                            -- N CLOCK CYCLES TO WRITE FIFO (CONSTANT)
            m_writing_counter<= m_writing_counter+1;
            -- filling write fifo
  
        elsif (m_writing_counter = CC_WRITE_FIFO ) then
            m_fifoW_rden    <='0';  -- stop reading write fifo
            im_reading      <='0';
            m_LPC_LEDS_SC   <= "0000000000";
            m_WR_start      <='0';
            m_fifoR_reset   <='0';
            m_write_to_ipbus_done<='0';
            m_writing_counter <= m_writing_counter+1;
            SET_VCASN         <= m_WWORD_full(127 downTo 0);
            SET_VCLIP         <= m_WWORD_full(255 downTo 128);
            SET_VPLSE_HIGH    <= m_WWORD_full(383 downTo 256);
            SET_VPLSE_LOW     <= m_WWORD_full(511 downTo 384);
            SET_VRESET_P      <= m_WWORD_full(639 downTo 512);
            SET_VRESET_D      <= m_WWORD_full(767 downTo 640);
            SET_ICASN         <= m_WWORD_full(895 downTo 768);
            SET_IRESET        <= m_WWORD_full(1023 downTo 896);
            SET_ITHR          <= m_WWORD_full(1151 downTo 1024);
            SET_IBIAS         <= m_WWORD_full(1279 downTo 1152);
            SET_IDB           <= m_WWORD_full(1407 downTo 1280);
            MASK_COL          <= m_WWORD_full(1919 downTo 1408);
            MASK_HOR          <= m_WWORD_full(2431 downTo 1920);
            MASK_DIAG         <= m_WWORD_full(2943 downTo 2432);
            MASK_FULLCOL      <= m_WWORD_full(3199 downTo 2944);
            PULSE_COL         <= m_WWORD_full(3711 downTo 3200);
            PULSE_HOR         <= m_WWORD_full(4223 downTo 3712);
        elsif (m_writing_counter=1 + CC_WRITE_FIFO ) then
            m_LPC_LEDS_SC <= "0000000001";
            m_writing_counter<= m_writing_counter+1;
        elsif (m_writing_counter=2 + CC_WRITE_FIFO ) then
            m_LPC_LEDS_SC <= "0000000101";
            
            m_writing_counter<= m_writing_counter+1;
            
        elsif (m_writing_counter= 3 + CC_WRITE_FIFO ) then        -- send start write command (should take 1 cc)
            m_WR_start  <='1';
            m_writing_counter<= m_writing_counter+1;
        elsif ( m_writing_counter < 4325 + CC_WRITE_FIFO ) then -- (LF < 4326) writting time 4324+3
            m_WR_start<='0';
            im_writing<='1';
            m_writing_counter<= m_writing_counter+1;
        elsif ( m_writing_counter = 4325 + CC_WRITE_FIFO ) then --(LF = 4326)
            im_writing<='0';
            m_R_start <='1';-- wait for clock falling edge
            im_reading<='0';
            m_writing_counter<= m_writing_counter+1;
        elsif ( m_writing_counter < 8648 + CC_WRITE_FIFO ) then --  (LF < 8647) writting time 8644+3
            m_R_start <='0';
            im_reading<='1';
            m_LPC_LEDS_SC <= "1010000000";
            m_writing_counter<= m_writing_counter+1;
        elsif ( m_writing_counter = 8648 + CC_WRITE_FIFO ) then --(LF < 8648) writting time #8644+1+3
            m_LPC_LEDS_SC <= "1011100000";
            m_R_start           <='0';  -- !!!!FIND RIGHT TIMING!!!!!Start deserializer. 
            im_reading<='0';
            
            R_DATAFLOW_MERGERTOLEFT                   <= m_SC_data_toUser(0);
            R_DATAFLOW_MERGERTORIGHT                  <= m_SC_data_toUser(1);
            R_DATAFLOW_LMERGERTOLVDS                  <= m_SC_data_toUser(2);
            R_DATAFLOW_LMERGERTOCMOS                  <= m_SC_data_toUser(3);
            R_DATAFLOW_RMERGERTOLVDS                  <= m_SC_data_toUser(4);
            R_DATAFLOW_RMERGERTOCMOS                  <= m_SC_data_toUser(5);
            R_DATAFLOW_LCMOS                          <= m_SC_data_toUser(6);
            R_DATAFLOW_RCMOS                          <= m_SC_data_toUser(7);
            R_DATAFLOW_ENLVDS                         <= m_SC_data_toUser(8);
            R_DATAFLOW_ENLCMOS                        <= m_SC_data_toUser(9);
            R_DATAFLOW_ENRCMOS                        <= m_SC_data_toUser(10);
            R_DATAFLOW_ENMERGER                       <= m_SC_data_toUser(11);
            R_POWER_SWITCH_LEFT                       <= m_SC_data_toUser(12);
            R_POWER_SWITCH_RIGHT                      <= m_SC_data_toUser(13);
            R_LVDS_EN                                 <= m_SC_data_toUser(14);
            R_LVDS_PRE                                <= m_SC_data_toUser(30 downTo 15);
            R_LVDS_BRIDGE_EN                          <= m_SC_data_toUser(35 downTo 31);
            R_LVDS_CMFB_EN                            <= m_SC_data_toUser(40 downTo 36);
            R_LVDS_SET_IBCMFB                         <= m_SC_data_toUser(44 downTo 41);
            R_LVDS_SET_IVPH                           <= m_SC_data_toUser(48 downTo 45);
            R_LVDS_SET_IVPL                           <= m_SC_data_toUser(52 downTo 49);
            R_LVDS_SET_IVNH                           <= m_SC_data_toUser(56 downTo 53);
            R_LVDS_SET_IVNL                           <= m_SC_data_toUser(60 downTo 57);
            R_SWCNTL_VCASN                            <= m_SC_data_toUser(61);
            R_SWCNTL_VCLIP                            <= m_SC_data_toUser(62);
            R_SWCNTL_VPLSE_HIGH                       <= m_SC_data_toUser(63);
            R_SWCNTL_VPLSE_LOW                        <= m_SC_data_toUser(64);
            R_SWCNTL_VRESET_P                         <= m_SC_data_toUser(65);
            R_SWCNTL_VRESET_D                         <= m_SC_data_toUser(66);
            R_SWCNTL_ICASN                            <= m_SC_data_toUser(67);
            R_SWCNTL_IRESET                           <= m_SC_data_toUser(68);
            R_SWCNTL_IBIAS                            <= m_SC_data_toUser(69);
            R_SWCNTL_ITHR                             <= m_SC_data_toUser(70);
            R_SWCNTL_IDB                              <= m_SC_data_toUser(71);
            R_SWCNTL_IREF                             <= m_SC_data_toUser(72);
            R_SET_IRESET_BIT                          <= m_SC_data_toUser(73);
            R_SWCNTL_DACMONV                          <= m_SC_data_toUser(74);
            R_SWCNTL_DACMONI                          <= m_SC_data_toUser(75);
            R_SET_IBUFP_MON_0                         <= m_SC_data_toUser(79 downTo 76);
            R_SET_IBUFN_MON_0                         <= m_SC_data_toUser(83 downTo 80);
            R_SET_IBUFP_MON_1                         <= m_SC_data_toUser(87 downTo 84);
            R_SET_IBUFN_MON_1                         <= m_SC_data_toUser(91 downTo 88);
            R_PULSE_MON_L                             <= m_SC_data_toUser(4316);
            R_PULSE_MON_R                             <= m_SC_data_toUser(4317);
            R_LVDS_VBCMFB                             <= m_SC_data_toUser(4321 downTo 4318);
            
            
            m_RWORD_full <= m_SC_data_toUser(4315 downTo 92);
            m_writing_counter<= m_writing_counter+1;      
        elsif ( m_writing_counter = 8649 + CC_WRITE_FIFO ) then --(LF < 4649) writting time  8644+2+3
            m_writing_counter<= m_writing_counter+1;
        else-- time to put memory into fifo
            if (m_writing_fifo_counter < CC_WRITE_FIFO ) then 
                m_writing_fifo_counter <= m_writing_fifo_counter+1;
            else
            end if;
            case m_writing_fifo_counter is
                when 18 =>
                    m_fifoR_wren            <='0';
                when 17 => 
                    m_RWORD(127 downTo 0)        <=(others => '0');-- THIS IS NECESSARY TO AVOID SENDING THE PREIVOUSLY DATA FROM LAST CC!!!
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 4223 downTo 4192);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 4191 downTo 4160);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 4159 downTo 4128);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 4127 downTo 4096);
                when 16 => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 4095 downTo 4064);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 4063 downTo 4032);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 4031 downTo 4000);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 3999 downTo 3968);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 3967 downTo 3936);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 3935 downTo 3904);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 3903 downTo 3872);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 3871 downTo 3840); 
                when 15 => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 3839 downTo 3808);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 3807 downTo 3776);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 3775 downTo 3744);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 3743 downTo 3712);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 3711 downTo 3680);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 3679 downTo 3648);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 3647 downTo 3616);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 3615 downTo 3584);
                when 14 => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 3583 downTo 3552);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 3551 downTo 3520);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 3519 downTo 3488);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 3487 downTo 3456);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 3455 downTo 3424);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 3423 downTo 3392);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 3391 downTo 3360);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 3359 downTo 3328);
                when 13 => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 3327 downTo 3296);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 3295 downTo 3264);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 3263 downTo 3232);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 3231 downTo 3200);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 3199 downTo 3168);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 3167 downTo 3136);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 3135 downTo 3104);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 3103 downTo 3072);
                when 12 => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 3071 downTo 3040);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 3039 downTo 3008);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 3007 downTo 2976);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 2975 downTo 2944);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 2943 downTo 2912);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 2911 downTo 2880);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 2879 downTo 2848);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 2847 downTo 2816);
                when 11 => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 2815 downTo 2784);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 2783 downTo 2752);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 2751 downTo 2720);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 2719 downTo 2688);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 2687 downTo 2656);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 2655 downTo 2624);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 2623 downTo 2592);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 2591 downTo 2560);
                when 10  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 2559 downTo 2528);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 2527 downTo 2496);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 2495 downTo 2464);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 2463 downTo 2432);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 2431 downTo 2400);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 2399 downTo 2368);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 2367 downTo 2336);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 2335 downTo 2304);
                when 9  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 2303 downTo 2272);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 2271 downTo 2240);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 2239 downTo 2208);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 2207 downTo 2176);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 2175 downTo 2144);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 2143 downTo 2112);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 2111 downTo 2080);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 2079 downTo 2048);
                when 8  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 2047 downTo 2016);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 2015 downTo 1984);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 1983 downTo 1952);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 1951 downTo 1920);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 1919 downTo 1888);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 1887 downTo 1856);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 1855 downTo 1824);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 1823 downTo 1792);
                when 7  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 1791 downTo 1760);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 1759 downTo 1728);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 1727 downTo 1696);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 1695 downTo 1664);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 1663 downTo 1632);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 1631 downTo 1600);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 1599 downTo 1568);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 1567 downTo 1536);
                when 6  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 1535 downTo 1504);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 1503 downTo 1472);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 1471 downTo 1440);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 1439 downTo 1408);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 1407 downTo 1376);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 1375 downTo 1344);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 1343 downTo 1312);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 1311 downTo 1280);
                when 5  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 1279 downTo 1248);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 1247 downTo 1216);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 1215 downTo 1184);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 1183 downTo 1152);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 1151 downTo 1120);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 1119 downTo 1088);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 1087 downTo 1056);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 1055 downTo 1024);
                when 4  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 1023 downTo 992);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 991 downTo 960);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 959 downTo 928);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 927 downTo 896);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 895 downTo 864);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 863 downTo 832);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 831 downTo 800);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 799 downTo 768); 
                when 3  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 767 downTo 736);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 735 downTo 704);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 703 downTo 672);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 671 downTo 640);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 639 downTo 608);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 607 downTo 576);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 575 downTo 544) ;
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 543 downTo 512);
                when 2  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 511 downTo 480) ;
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 479 downTo 448);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 447 downTo 416);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 415 downTo 384);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 383 downTo 352);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 351 downTo 320) ;
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 319 downTo 288);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 287 downTo 256); 
                when 1  => 
                    m_RWORD( 31 downTo 0)        <= m_RWORD_full ( 255 downTo 224);
                    m_RWORD( 63 downTo 32)       <= m_RWORD_full ( 223 downTo 192);
                    m_RWORD( 95 downTo 64)       <= m_RWORD_full ( 191 downTo 160);
                    m_RWORD( 127 downTo 96)      <= m_RWORD_full ( 159 downTo 128);
                    m_RWORD( 159 downTo 128)     <= m_RWORD_full ( 127 downTo 96);
                    m_RWORD( 191 downTo 160)     <= m_RWORD_full ( 95 downTo 64);
                    m_RWORD( 223 downTo 192)     <= m_RWORD_full ( 63 downTo 32);
                    m_RWORD( 255 downTo 224)     <= m_RWORD_full ( 31 downTo 0); 
                    m_fifoR_wren   <='1';
                when 0  => 
                    m_fifoR_wren   <='0';
                when others =>
                    m_RWORD       <= (others => '0');
                    m_fifoR_wren            <='0';
                    m_LPC_LEDS_SC           <="1111111111";
            end case;
        end if;           
    end if;
  end if;
end process;
 


-- Read fifo
FIFOR_R_WORD : fifo_SC_toUser256
  port map(
      rst           => m_fifoR_reset,
      wr_clk        => i_clk_sc,-- DO NOT CHANGE THIS EVER
      rd_clk        => i_clk_IPb,-- DO NOT CHANGE THIS EVER
      din           => m_RWORD,
      wr_en         => m_fifoR_wren,
      rd_en         => m_fifoR_rden,
      dout          => m_fifoR_RWORD_reg ,
      full          => m_fifoR_full,
      almost_full   => m_fifoR_almostFull,
      empty         => m_fifoR_empty
  );


--Register 59: SlowControl FIFO_R
slowcontrol_r: entity work.ipbus_fifo
generic map(
c_ok_in_msb => '0'
)
port map(
    c_address  => std_logic_vector(to_unsigned(IPBADDR_RWORD,32)), 
    i_clk      => i_clk_IPb,
    i_ipbw     => i_ipbw(IPBADDR_RWORD),
    o_ipbr     => i_ipbr(IPBADDR_RWORD),
    c_readonly => '1',
    i_empty    => m_fifoR_empty,
    i_full     => m_fifoR_full, -- useless
    i_rddata   => m_fifoR_RWORD_reg,
    --o_rdclk    => m_fifoR_rdclk,
    o_rdenable => m_fifoR_rden,
    o_wrdata   => open, -- useless
    o_wrclk    => open, -- useless
    o_wrenable => open -- useless
); 





------------------------------------------------------------------------------------
 -- ALL IPBUS REGISTERS TO WRITE INTO MALTA2    
 reg_control: entity work.ipbus_register
 port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_SCCONTROL,32)),
   i_ipbw     => i_ipbw(IPBADDR_SCCONTROL),
   o_ipbr     => i_ipbr(IPBADDR_SCCONTROL),
   i_clk      => i_clk_IPb,  i_reset    => i_IPBeset,
   c_readonly => '0'      , --c_writeMon => '1',
   i_default  => X"00000000",
   o_signal   => m_control
 );
 m_SC_state <= m_control(2 downTo 0);


-- SMALL IPBUS REGISTYERS---------------------------------------------
-- Register: reg_DATAFLOW_MERGERTOLEFT
-- SMALL IPBUS REGISTYERS---------------------------------------------
-- Register: reg_DATAFLOW_MERGERTOLEFT

----------------New IPBus registers added by Leyre for simple registers---------------------

-- Register: reg_DATAFLOW_SWITCH_LVDS-EN-PRE
 reg_DATAFLOW_SWITCH_LVDS_EN_PRE: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_SC_SMALL_1_W,32)),
 i_ipbw     => i_ipbw(IPBADDR_SC_SMALL_1_W),
 o_ipbr     => i_ipbr(IPBADDR_SC_SMALL_1_W),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '0'        , --c_writeMon => '1',
 i_default  => m_SC_SMALL_1_W  , o_signal => m_SC_SMALL_1_W 
 );
DATAFLOW_MERGERTOLEFT   <= m_SC_SMALL_1_W(0);        
DATAFLOW_MERGERTORIGHT  <= m_SC_SMALL_1_W(1);        
DATAFLOW_LMERGERTOLVDS  <= m_SC_SMALL_1_W(2);         
DATAFLOW_LMERGERTOCMOS  <= m_SC_SMALL_1_W(3);          
DATAFLOW_RMERGERTOLVDS  <= m_SC_SMALL_1_W(4);          
DATAFLOW_RMERGERTOCMOS  <= m_SC_SMALL_1_W(5);          
DATAFLOW_LCMOS          <= m_SC_SMALL_1_W(6);        
DATAFLOW_RCMOS          <= m_SC_SMALL_1_W(7);        
DATAFLOW_ENLVDS         <= m_SC_SMALL_1_W(8);        
DATAFLOW_ENLCMOS        <= m_SC_SMALL_1_W(9);        
DATAFLOW_ENRCMOS        <= m_SC_SMALL_1_W(10);        
DATAFLOW_ENMERGER       <= m_SC_SMALL_1_W(11);        
POWER_SWITCH_LEFT       <= m_SC_SMALL_1_W(12);        
POWER_SWITCH_RIGHT      <= m_SC_SMALL_1_W(13);        
LVDS_EN                 <= m_SC_SMALL_1_W(14);       
LVDS_PRE                <= m_SC_SMALL_1_W(30 downto 15);                     


-- Register: reg_LVDS_BRIDGE_CMF_IB_IV
 reg_LVDS_BRIDGE_CMF_IB_IV: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_LVDS_BRIDGE_CMF_IB_IV,32)),
 i_ipbw     => i_ipbw(IPBADDR_LVDS_BRIDGE_CMF_IB_IV),
 o_ipbr     => i_ipbr(IPBADDR_LVDS_BRIDGE_CMF_IB_IV),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '0'        , --c_writeMon => '1',
 i_default  => m_LVDS_BRIDGE_CMF_IB_IV  , o_signal => m_LVDS_BRIDGE_CMF_IB_IV
);

LVDS_BRIDGE_EN      <= m_LVDS_BRIDGE_CMF_IB_IV(4 downto 0);
LVDS_CMFB_EN        <= m_LVDS_BRIDGE_CMF_IB_IV(9 downto 5);
LVDS_SET_IBCMFB     <= m_LVDS_BRIDGE_CMF_IB_IV(13 downto 10);
LVDS_SET_IVPH       <= m_LVDS_BRIDGE_CMF_IB_IV(17 downto 14);
LVDS_SET_IVPL       <= m_LVDS_BRIDGE_CMF_IB_IV(21 downto 18);
LVDS_SET_IVNH       <= m_LVDS_BRIDGE_CMF_IB_IV(25 downto 22);
LVDS_SET_IVNL       <= m_LVDS_BRIDGE_CMF_IB_IV(29 downto 26);

-- Register: reg_SWCNTL_IBUFMON
 reg_SWCNTL_IBUFMON: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_SWCNTL_IBUFMON,32)),
 i_ipbw     => i_ipbw(IPBADDR_SWCNTL_IBUFMON),
 o_ipbr     => i_ipbr(IPBADDR_SWCNTL_IBUFMON),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '0'        , --c_writeMon => '1',
 i_default  => m_SWCNTL_IBUFMON  , o_signal => m_SWCNTL_IBUFMON
);

SWCNTL_VCASN        <= m_SWCNTL_IBUFMON(0);
SWCNTL_VCLIP        <= m_SWCNTL_IBUFMON(1);
SWCNTL_VPLSE_HIGH   <= m_SWCNTL_IBUFMON(2);
SWCNTL_VPLSE_LOW    <= m_SWCNTL_IBUFMON(3);
SWCNTL_VRESET_P     <= m_SWCNTL_IBUFMON(4);
SWCNTL_VRESET_D     <= m_SWCNTL_IBUFMON(5);
SWCNTL_ICASN        <= m_SWCNTL_IBUFMON(6);
SWCNTL_IRESET       <= m_SWCNTL_IBUFMON(7);
SWCNTL_IBIAS        <= m_SWCNTL_IBUFMON(8);
SWCNTL_ITHR         <= m_SWCNTL_IBUFMON(9);
SWCNTL_IDB          <= m_SWCNTL_IBUFMON(10);
SWCNTL_IREF         <= m_SWCNTL_IBUFMON(11);
SET_IRESET_BIT      <= m_SWCNTL_IBUFMON(12);
SWCNTL_DACMONV      <= m_SWCNTL_IBUFMON(13);
SWCNTL_DACMONI      <= m_SWCNTL_IBUFMON(14);
SET_IBUFP_MON_0     <= m_SWCNTL_IBUFMON(18 downto 15);  
SET_IBUFN_MON_0     <= m_SWCNTL_IBUFMON(22 downto 19);
SET_IBUFP_MON_1     <= m_SWCNTL_IBUFMON(26 downto 23);
SET_IBUFN_MON_1     <= m_SWCNTL_IBUFMON(30 downto 27);

-- Register: reg_PULSE_MON_DELAY
 reg_PULSE_MON_DELAY: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_PULSE_MON_DELAY,32)),
 i_ipbw     => i_ipbw(IPBADDR_PULSE_MON_DELAY),
 o_ipbr     => i_ipbr(IPBADDR_PULSE_MON_DELAY),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '0'        , --c_writeMon => '1',
 i_default  => m_PULSE_MON_DELAY  , o_signal => m_PULSE_MON_DELAY
);

PULSE_MON_L         <= m_PULSE_MON_DELAY(0);
PULSE_MON_R         <= m_PULSE_MON_DELAY(1);
LVDS_VBCMFB         <= m_PULSE_MON_DELAY(5 downto 2);

-- Same register but for Read so name_R

-- Register: reg_DATAFLOW_SWITCH_LVDS_EN_PRE_R
 reg_DATAFLOW_SWITCH_LVDS_EN_PRE_R: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_SC_SMALL_1_R,32)),
 i_ipbw     => i_ipbw(IPBADDR_SC_SMALL_1_R),
 o_ipbr     => i_ipbr(IPBADDR_SC_SMALL_1_R),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '1'        , --c_writeMon => '0',
 i_default  => m_SC_SMALL_1_R  , o_signal => open 
);
m_SC_SMALL_1_R(0)            <= R_DATAFLOW_MERGERTOLEFT;
m_SC_SMALL_1_R(1)            <= R_DATAFLOW_MERGERTORIGHT;        
m_SC_SMALL_1_R(2)            <= R_DATAFLOW_LMERGERTOLVDS;         
m_SC_SMALL_1_R(3)            <= R_DATAFLOW_LMERGERTOCMOS;          
m_SC_SMALL_1_R(4)            <= R_DATAFLOW_RMERGERTOLVDS;          
m_SC_SMALL_1_R(5)            <= R_DATAFLOW_RMERGERTOCMOS;          
m_SC_SMALL_1_R(6)            <= R_DATAFLOW_LCMOS;        
m_SC_SMALL_1_R(7)            <= R_DATAFLOW_RCMOS;        
m_SC_SMALL_1_R(8)            <= R_DATAFLOW_ENLVDS;        
m_SC_SMALL_1_R(9)            <= R_DATAFLOW_ENLCMOS;        
m_SC_SMALL_1_R(10)           <= R_DATAFLOW_ENRCMOS;        
m_SC_SMALL_1_R(11)           <= R_DATAFLOW_ENMERGER;        
m_SC_SMALL_1_R(12)           <= R_POWER_SWITCH_LEFT;        
m_SC_SMALL_1_R(13)           <= R_POWER_SWITCH_RIGHT;        
m_SC_SMALL_1_R(14)           <= R_LVDS_EN;       
m_SC_SMALL_1_R(30 downto 15) <= R_LVDS_PRE;   

-- Register: reg_LVDS_BRIDGE_CMF_IB_IV_R
 reg_LVDS_BRIDGE_CMF_IB_IV_R: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_LVDS_BRIDGE_CMF_IB_IV_R,32)),
 i_ipbw     => i_ipbw(IPBADDR_LVDS_BRIDGE_CMF_IB_IV_R),
 o_ipbr     => i_ipbr(IPBADDR_LVDS_BRIDGE_CMF_IB_IV_R),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '1'        ,-- c_writeMon => '1',
 i_default  => m_LVDS_BRIDGE_CMF_IB_IV_R  , o_signal => open
);

m_LVDS_BRIDGE_CMF_IB_IV_R(4 downto 0)       <= R_LVDS_BRIDGE_EN;
m_LVDS_BRIDGE_CMF_IB_IV_R(9 downto 5)       <= R_LVDS_CMFB_EN;       
m_LVDS_BRIDGE_CMF_IB_IV_R(13 downto 10)     <= R_LVDS_SET_IBCMFB; 
m_LVDS_BRIDGE_CMF_IB_IV_R(17 downto 14)     <= R_LVDS_SET_IVPH; 
m_LVDS_BRIDGE_CMF_IB_IV_R(21 downto 18)     <= R_LVDS_SET_IVPL;
m_LVDS_BRIDGE_CMF_IB_IV_R(25 downto 22)     <= R_LVDS_SET_IVNH;
m_LVDS_BRIDGE_CMF_IB_IV_R(29 downto 26)     <= R_LVDS_SET_IVNL;

-- Register: reg_SWCNTL_IBUFMON_R
 reg_SWCNTL_IBUFMON_R: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_SWCNTL_IBUFMON_R,32)),
 i_ipbw     => i_ipbw(IPBADDR_SWCNTL_IBUFMON_R),
 o_ipbr     => i_ipbr(IPBADDR_SWCNTL_IBUFMON_R),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '1'        , --c_writeMon => '1',
 i_default  => m_SWCNTL_IBUFMON_R  , o_signal => open
);
m_SWCNTL_IBUFMON_R(0)               <= R_SWCNTL_VCASN;
m_SWCNTL_IBUFMON_R(1)               <= R_SWCNTL_VCLIP;
m_SWCNTL_IBUFMON_R(2)               <= R_SWCNTL_VPLSE_HIGH;
m_SWCNTL_IBUFMON_R(3)               <= R_SWCNTL_VPLSE_LOW;
m_SWCNTL_IBUFMON_R(4)               <= R_SWCNTL_VRESET_P;
m_SWCNTL_IBUFMON_R(5)               <= R_SWCNTL_VRESET_D;
m_SWCNTL_IBUFMON_R(6)               <= R_SWCNTL_ICASN;
m_SWCNTL_IBUFMON_R(7)               <= R_SWCNTL_IRESET;
m_SWCNTL_IBUFMON_R(8)               <= R_SWCNTL_IBIAS;
m_SWCNTL_IBUFMON_R(9)               <= R_SWCNTL_ITHR;
m_SWCNTL_IBUFMON_R(10)              <= R_SWCNTL_IDB;
m_SWCNTL_IBUFMON_R(11)              <= R_SWCNTL_IREF;
m_SWCNTL_IBUFMON_R(12)              <= R_SET_IRESET_BIT;
m_SWCNTL_IBUFMON_R(13)              <= R_SWCNTL_DACMONV;
m_SWCNTL_IBUFMON_R(14)              <= R_SWCNTL_DACMONI;
m_SWCNTL_IBUFMON_R(18 downto 15)    <= R_SET_IBUFP_MON_0;  
m_SWCNTL_IBUFMON_R(22 downto 19)    <= R_SET_IBUFN_MON_0;
m_SWCNTL_IBUFMON_R(26 downto 23)    <= R_SET_IBUFP_MON_1;
m_SWCNTL_IBUFMON_R(30 downto 27)    <= R_SET_IBUFN_MON_1;

-- Register: reg_PULSE_MON_DELAY_R
 reg_PULSE_MON_DELAY_R: entity work.ipbus_register
 port map(
 c_address  => std_logic_vector(to_unsigned(IPBADDR_PULSE_MON_DELAY_R,32)),
 i_ipbw     => i_ipbw(IPBADDR_PULSE_MON_DELAY_R),
 o_ipbr     => i_ipbr(IPBADDR_PULSE_MON_DELAY_R),
 i_clk      => i_clk_IPb, i_reset    => i_IPBeset,
 c_readonly => '1'        , --c_writeMon => '1',
 i_default  => m_PULSE_MON_DELAY_R  , o_signal => open
);

m_PULSE_MON_DELAY_R(0)          <= R_PULSE_MON_L;
m_PULSE_MON_DELAY_R(1)          <= R_PULSE_MON_R;
m_PULSE_MON_DELAY_R(5 downto 2) <= R_LVDS_VBCMFB;



reg_SPLIT_REG: entity work.ipbus_register
 port map(
   c_address  => std_logic_vector(to_unsigned(IPBADDR_SPLIT_REG,32)),
   i_ipbw     => i_ipbw(IPBADDR_SPLIT_REG),
   o_ipbr     => i_ipbr(IPBADDR_SPLIT_REG),
   i_clk      => i_clk_IPb,  i_reset    => i_IPBeset,
   c_readonly => '0'      , --c_writeMon => '1',
   i_default  => X"00000000",
   o_signal   => m_split
 );
 
 
-----------------------------------------------------------------------------------------------------------------------------------------------------------
o_LPC_LEDS <= m_LPC_LEDS_SC;
end Behavioral;


