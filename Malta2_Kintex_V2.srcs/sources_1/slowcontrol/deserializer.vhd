library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.package_constants.all;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

entity deserializer is
port(
    i_clock        : in  std_logic;
    o_dataParallel : out std_logic_vector(SCNBITS downto 0);
    i_dataSerial   : in  std_logic;
    o_busy         : out std_logic;
    i_startR       : in  std_logic 
);
end deserializer;

----------------------------------------------------
----------------------------------------------------
----------------------------------------------------

architecture ARCH1_Deserializer of deserializer is

    signal m_i_clock        : std_logic := '0';
    signal m_o_busy         : std_logic := '0';
    signal m_i_dataSerial   : std_logic := '0';
    signal m_o_dataParallel : std_logic_vector(SCNBITS downto 0):= (others => '0');
    signal m_start          : std_logic := '0';
    
    signal m_counter_bitsInWord : natural range 0 to SCNBITS := 0;

begin
    -- inputs
    m_i_clock      <= i_clock;
    m_i_dataSerial <= i_dataSerial;
    m_start        <= i_startR;
   
    -- outputs
    o_busy         <= m_o_busy;
    o_dataParallel <= m_o_dataParallel;

    process_deserializer : process (i_clock) is
    begin
        if rising_edge(i_clock) then
            
            if m_start='0' and m_counter_bitsInWord=0 then
               m_counter_bitsInWord<=0;
               m_o_busy<='0'; 
               --m_o_dataParallel<= (others => '1'); --unclear that this is needed .... I would stick to the last word
            elsif m_start='1' and m_counter_bitsInWord=0 then
               m_o_busy <= '1';
               m_o_dataParallel(m_counter_bitsInWord) <= m_i_dataSerial;
               m_counter_bitsInWord <= m_counter_bitsInWord + 1;
            else 
               if m_counter_bitsInWord = SCNBITS then
                  m_o_busy <= '0';
                  m_counter_bitsInWord <= 0;
                  m_o_dataParallel(m_counter_bitsInWord) <= m_i_dataSerial;-- DO NOT REMOVE THIS LINE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               else
                  m_o_busy <= '1';
                  m_o_dataParallel(m_counter_bitsInWord) <= m_i_dataSerial;
                  m_counter_bitsInWord <= m_counter_bitsInWord + 1;
               end if;
            end if;

        end if;
    end process process_deserializer;


end ARCH1_Deserializer;






--architecture ARCH2_Deserializer of deserializer is

--    --------
--    -- wires
--    --------

--    signal m_i_clock        : std_logic := '0';
--    signal m_o_busy         : std_logic := '0';
--    signal m_i_dataSerial   : std_logic := '1';
--    signal m_o_dataParallel : std_logic_vector(SCNBITS downto 0);--X"0000";
--    signal m_do          : std_logic := '0';
--    -----------------------
--    -- counter: SCNBITS bit word
--    -----------------------

--    signal m_counter_bitsInWord : natural range 0 to SCNBITS := 0;

--begin

--    ----------
--    -- process
--    ----------

--    process_deserializer : process (i_clock) is
--    begin
--        if rising_edge(i_clock) then

--            if m_do = '0' then
--                m_o_dataParallel<= (others => '1');
--                m_o_busy <= '1';
--                m_counter_bitsInWord <= 0;
--            else
--                if m_counter_bitsInWord = SCNBITS then -- last clock cycle used to unset busy and reset bit counter
--                    m_o_busy <= '1';
--                    m_counter_bitsInWord <= 0;
--                    m_o_dataParallel(m_counter_bitsInWord) <= m_i_dataSerial;
--                else
--                    m_o_busy <= '0';
--                    m_o_dataParallel(m_counter_bitsInWord) <= m_i_dataSerial;
--                    m_counter_bitsInWord <= m_counter_bitsInWord + 1;
--                end if;
--            end if;

--        end if;
--    end process process_deserializer;

--    ---------
--    -- inputs
--    ---------

--    m_i_clock      <= i_clock;
--    m_i_dataSerial <= i_dataSerial;
--    m_do <= i_startR;
--    ----------
--    -- outputs
--    ----------

--    o_busy         <= m_o_busy;
--    o_dataParallel <= m_o_dataParallel;

--end ARCH2_Deserializer;
