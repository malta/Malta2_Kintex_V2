########################################################################################################################
#FMC2 MONITORING #######################################################################################################

###### Abhi's FMC
## LEDs
#LA33_N,#LA33_P,   .....  #LA22_P
set_property PACKAGE_PIN AC30 [get_ports {o_FMC2_LED[0]}]
set_property PACKAGE_PIN AC29 [get_ports {o_FMC2_LED[1]}]
set_property PACKAGE_PIN AE29 [get_ports {o_FMC2_LED[2]}]
set_property PACKAGE_PIN AD29 [get_ports {o_FMC2_LED[3]}]
set_property PACKAGE_PIN AF28 [get_ports {o_FMC2_LED[4]}]
set_property PACKAGE_PIN AE28 [get_ports {o_FMC2_LED[5]}]
set_property PACKAGE_PIN AD26 [get_ports {o_FMC2_LED[6]}]
set_property PACKAGE_PIN AC26 [get_ports {o_FMC2_LED[7]}]
set_property PACKAGE_PIN AK28 [get_ports {o_FMC2_LED[8]}]
set_property PACKAGE_PIN AJ27 [get_ports {o_FMC2_LED[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_LED[*]}]


## SMAs
# first N then P
# LA02, LA06, LA18_CC, LA00_CC, LA01_CC
set_property PACKAGE_PIN AF20 [get_ports {o_FMC2_SMA[0]}]
set_property PACKAGE_PIN AF21 [get_ports {o_FMC2_SMA[1]}]
set_property PACKAGE_PIN AD27 [get_ports {o_FMC2_SMA[2]}]
set_property PACKAGE_PIN AD28 [get_ports {o_FMC2_SMA[3]}]
set_property PACKAGE_PIN AD23 [get_ports {o_FMC2_SMA[4]}]
set_property PACKAGE_PIN AE24 [get_ports {o_FMC2_SMA[5]}]
set_property PACKAGE_PIN AK20 [get_ports {o_FMC2_SMA[6]}]
set_property PACKAGE_PIN AK21 [get_ports {o_FMC2_SMA[7]}]
set_property PACKAGE_PIN AE23 [get_ports {o_FMC2_SMA[8]}]
set_property PACKAGE_PIN AF23 [get_ports {o_FMC2_SMA[9]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[2]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[8]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_FMC2_SMA[9]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[2]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[3]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[4]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[5]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[6]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[7]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[8]}]
#set_property IOSTANDARD LVDS_25 [get_ports {o_FMC2_SMA[9]}]


