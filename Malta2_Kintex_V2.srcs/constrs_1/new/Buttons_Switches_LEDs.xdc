#Pushbutton
set_property PACKAGE_PIN AA12 [get_ports i_GPIO_SW_N]
set_property PACKAGE_PIN AB12 [get_ports i_GPIO_SW_S]
set_property IOSTANDARD LVCMOS15 [get_ports i_GPIO_SW_N]
set_property IOSTANDARD LVCMOS15 [get_ports i_GPIO_SW_S]

#Switches
set_property PACKAGE_PIN Y29 [get_ports {i_swi[0]}]
set_property PACKAGE_PIN W29 [get_ports {i_swi[1]}]
set_property PACKAGE_PIN AA28 [get_ports {i_swi[2]}]
#set_property PACKAGE_PIN BA32 [get_ports {i_swi[3]}]
#set_property PACKAGE_PIN AW30 [get_ports {i_swi[4]}]
#set_property PACKAGE_PIN AY30 [get_ports {i_swi[5]}]
#set_property PACKAGE_PIN BA30 [get_ports {i_swi[6]}]
#set_property PACKAGE_PIN BB31 [get_ports {i_swi[7]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_swi[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_swi[1]}]
set_property IOSTANDARD LVCMOS25 [get_ports {i_swi[2]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[3]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[4]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[5]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[6]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[7]}]

#LED
set_property PACKAGE_PIN AB8 [get_ports {o_LED[0]}]
set_property PACKAGE_PIN AA8 [get_ports {o_LED[1]}]
set_property PACKAGE_PIN AC9 [get_ports {o_LED[2]}]
set_property PACKAGE_PIN AB9 [get_ports {o_LED[3]}]
set_property PACKAGE_PIN AE26 [get_ports {o_LED[4]}]
set_property PACKAGE_PIN G19 [get_ports {o_LED[5]}]
set_property PACKAGE_PIN E18 [get_ports {o_LED[6]}]
set_property PACKAGE_PIN F16 [get_ports {o_LED[7]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_LED[0]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_LED[1]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_LED[2]}]
set_property IOSTANDARD LVCMOS15 [get_ports {o_LED[3]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LED[4]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LED[5]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LED[6]}]
set_property IOSTANDARD LVCMOS25 [get_ports {o_LED[7]}]

#SMA cables
set_property PACKAGE_PIN Y23 [get_ports i_SMA_user_P]
set_property PACKAGE_PIN Y24 [get_ports o_SMA_user_N]
set_property IOSTANDARD LVCMOS25 [get_ports i_SMA_user_P]
set_property IOSTANDARD LVCMOS25 [get_ports o_SMA_user_N]

##set_property PACKAGE_PIN AB30    [get_ports o_VREF]
set_property PACKAGE_PIN C22 [get_ports o_VREF]
set_property IOSTANDARD LVCMOS25 [get_ports o_VREF]

















#### below froim Virtex
##Pushbutton
#set_property PACKAGE_PIN AR40 [get_ports i_GPIO_SW_N]
#set_property PACKAGE_PIN AP40 [get_ports i_GPIO_SW_S]
#set_property IOSTANDARD LVCMOS18 [get_ports i_GPIO_SW_N]
#set_property IOSTANDARD LVCMOS18 [get_ports i_GPIO_SW_S]

##Switches
#set_property PACKAGE_PIN AV30 [get_ports {i_swi[0]}]
#set_property PACKAGE_PIN AY33 [get_ports {i_swi[1]}]
#set_property PACKAGE_PIN BA31 [get_ports {i_swi[2]}]
##set_property PACKAGE_PIN BA32 [get_ports {i_swi[3]}]
##set_property PACKAGE_PIN AW30 [get_ports {i_swi[4]}]
##set_property PACKAGE_PIN AY30 [get_ports {i_swi[5]}]
##set_property PACKAGE_PIN BA30 [get_ports {i_swi[6]}]
##set_property PACKAGE_PIN BB31 [get_ports {i_swi[7]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[1]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[2]}]
##set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[3]}]
##set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[4]}]
##set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[5]}]
##set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[6]}]
##set_property IOSTANDARD LVCMOS18 [get_ports {i_swi[7]}]

##LED
#set_property PACKAGE_PIN AM39 [get_ports {o_LED[0]}]
#set_property PACKAGE_PIN AN39 [get_ports {o_LED[1]}]
#set_property PACKAGE_PIN AR37 [get_ports {o_LED[2]}]
#set_property PACKAGE_PIN AT37 [get_ports {o_LED[3]}]
#set_property PACKAGE_PIN AR35 [get_ports {o_LED[4]}]
#set_property PACKAGE_PIN AP41 [get_ports {o_LED[5]}]
#set_property PACKAGE_PIN AP42 [get_ports {o_LED[6]}]
#set_property PACKAGE_PIN AU39 [get_ports {o_LED[7]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[0]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[1]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[2]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[3]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[4]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[5]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[6]}]
#set_property IOSTANDARD LVCMOS18 [get_ports {o_LED[7]}]

##SMA cables
#set_property PACKAGE_PIN AN31 [get_ports i_SMA_user_P]
#set_property PACKAGE_PIN AP31 [get_ports o_SMA_user_N]
##set_property IOSTANDARD LVCMOS18 [get_ports i_SMA_user_P]
##set_property IOSTANDARD LVCMOS18 [get_ports o_SMA_user_N]
#set_property IOSTANDARD LVCMOS18 [get_ports i_SMA_user_P]
#set_property IOSTANDARD LVCMOS18 [get_ports o_SMA_user_N]

#set_property PACKAGE_PIN R32      [get_ports o_VREF]
#set_property IOSTANDARD  LVCMOS18 [get_ports o_VREF]












