set_property PACKAGE_PIN H14 [get_ports o_RST_TOMALTA]
set_property IOSTANDARD LVCMOS25 [get_ports o_RST_TOMALTA]

set_property PACKAGE_PIN F17 [get_ports o_TRIG_TOMALTA_N]
set_property PACKAGE_PIN G17 [get_ports o_TRIG_TOMALTA_P]
set_property PACKAGE_PIN B22 [get_ports o_VPULSE_TOMALTA_P]
set_property PACKAGE_PIN A22 [get_ports o_VPULSE_TOMALTA_N]

set_property IOSTANDARD LVDS_25 [get_ports o_TRIG_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_TRIG_TOMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports o_VPULSE_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_VPULSE_TOMALTA_N]

#RC added diff term on VPULSE and Trigger. moved from TRUE to trueset_property DIFF_TERM true [get_ports o_CLK_TOMALTA_N]
set_property DIFF_TERM TRUE [get_ports o_TRIG_TOMALTA_N]
set_property DIFF_TERM TRUE [get_ports o_TRIG_TOMALTA_P]
set_property DIFF_TERM TRUE [get_ports o_VPULSE_TOMALTA_N]
set_property DIFF_TERM TRUE [get_ports o_VPULSE_TOMALTA_P]

set_property PACKAGE_PIN H21 [get_ports i_SC_FROMMALTA_P]
set_property PACKAGE_PIN H22 [get_ports i_SC_FROMMALTA_N]
set_property PACKAGE_PIN B18 [get_ports o_SC_TOMALTA_P]
set_property PACKAGE_PIN A18 [get_ports o_SC_TOMALTA_N]
set_property PACKAGE_PIN E21 [get_ports o_SC_CLK_N]
set_property PACKAGE_PIN F21 [get_ports o_SC_CLK_P]

set_property IOSTANDARD LVDS_25 [get_ports i_SC_FROMMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports i_SC_FROMMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports o_SC_TOMALTA_P]
set_property IOSTANDARD LVDS_25 [get_ports o_SC_TOMALTA_N]
set_property IOSTANDARD LVDS_25 [get_ports o_SC_CLK_P]
set_property IOSTANDARD LVDS_25 [get_ports o_SC_CLK_N]

set_property DIFF_TERM TRUE [get_ports i_SC_FROMMALTA_N]
set_property DIFF_TERM TRUE [get_ports i_SC_FROMMALTA_P]
set_property DIFF_TERM TRUE [get_ports o_SC_TOMALTA_N]
set_property DIFF_TERM TRUE [get_ports o_SC_TOMALTA_P]
set_property DIFF_TERM TRUE [get_ports o_SC_CLK_N]
set_property DIFF_TERM TRUE [get_ports o_SC_CLK_P]


set_property PACKAGE_PIN F15 [get_ports o_SC_LOAD]
set_property IOSTANDARD LVCMOS25 [get_ports o_SC_LOAD]


