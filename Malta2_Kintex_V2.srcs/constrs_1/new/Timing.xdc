create_clock -period 5.000 -name SYSCLK_P -waveform {0.000 2.500} [get_ports SYSCLK_P]
create_clock -period 8.000 -name SGMIICLK_Q0_P -waveform {0.000 4.000} [get_ports SGMIICLK_Q0_P]


#set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets clipper/o_SMA_user_N_OBUF]


#create_generated_clock -name CLK_AO       -source [get_pins mainClk1/inst/mmcm_adv_inst/CLKIN1] -master_clock mainClk1/inst/clk_in1 [get_pins mainClk1/inst/mmcm_adv_inst/CLKOUT1]
#create_generated_clock -name CLK_SC       -source [get_pins mainClk1/inst/mmcm_adv_inst/CLKIN1] -master_clock mainClk1/inst/clk_in1 [get_pins mainClk1/inst/mmcm_adv_inst/CLKOUT4]
#create_generated_clock -name CLK_200_IDEL -source [get_pins mainClk2/inst/mmcm_adv_inst/CLKIN1] -master_clock mainClk2/inst/clk_in1 [get_pins mainClk2/inst/mmcm_adv_inst/CLKOUT1]
#create_generated_clock -name CLK_BCID     -source [get_pins mainClk2/inst/mmcm_adv_inst/CLKIN1] -master_clock mainClk2/inst/clk_in1 [get_pins mainClk2/inst/mmcm_adv_inst/CLKOUT3]

create_clock -period 3.125 -name CLK_AO -waveform {0.000 1.563} [get_pins mainClk1/clk_out1]
create_clock -period 3.125 -name CLK_AO_90 -waveform {0.781 2.344} [get_pins mainClk1/clk_out2]
create_clock -period 25.000 -name CLK_BCID -waveform {0.000 12.500} [get_pins mainClk1/clk_out3]
#create_clock -period 100.000 -name CLK_SC -waveform {0.000 50.000} [get_pins mainClk1/clk_out4]
create_clock -period 100.000 -name CLK_SC  [get_pins mainClk1/clk_out4]

create_clock -period 5.000 -name CLK_200_IDEL -waveform {0.000 2.500} [get_pins mainClk2/clk_out1]
create_clock -period 5.000 -name CLK_200_IPB -waveform {0.000 2.500} [get_pins mainClk2/clk_out2]

set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_AO]
set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_200_IDEL]
set_false_path -from [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]] -to [get_clocks CLK_SC]

set_false_path -from [get_clocks CLK_200_IDEL] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]
set_false_path -from [get_clocks CLK_AO] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]
set_false_path -from [get_clocks CLK_SC] -to [get_clocks [get_clocks -of_objects [get_pins ipbus/clocks/mmcm/CLKOUT1]]]

set_output_delay -clock CLK_SC 0.5 [get_ports o_SC_LOAD]
set_output_delay -clock CLK_SC 0.5 [get_ports {{o_SC_TOMALTA_P} {o_SC_TOMALTA_N}}]
set_output_delay -clock CLK_SC 0.5 [get_ports {{o_SC_CLK_P} {o_SC_CLK_N}}]

set_false_path -from [get_clocks -regexp .*] -to [get_ports {{o_LED[0]} {o_LED[1]} {o_LED[2]} {o_LED[3]} {o_LED[4]} {o_LED[5]} {o_LED[6]} {o_LED[7]}}]
set_false_path -from [get_clocks -regexp .*] -to [get_ports {{o_FMC2_LED[0]} {o_FMC2_LED[1]} {o_FMC2_LED[2]} {o_FMC2_LED[3]} {o_FMC2_LED[4]} {o_FMC2_LED[5]} {o_FMC2_LED[6]} {o_FMC2_LED[7]} {o_FMC2_LED[8]} {o_FMC2_LED[9]}}]
#set_false_path -from [get_clocks -regexp .*] -to [get_ports {{o_FMC2_SMA[0]} {o_FMC2_SMA[1]} {o_FMC2_SMA[2]} {o_FMC2_SMA[3]} {o_FMC2_SMA[4]} {o_FMC2_SMA[5]} {o_FMC2_SMA[6]} {o_FMC2_SMA[7]} {o_FMC2_SMA[8]} {o_FMC2_SMA[9]}}]

#set_property PACKAGE_PIN L25 [get_ports i_USER_SMA_CLOCK_P]
#set_property PACKAGE_PIN K25 [get_ports i_USER_SMA_CLOCK_N]
#set_property IOSTANDARD LVDS_25  [get_ports i_USER_SMA_CLOCK_P]
#set_property IOSTANDARD LVDS_25  [get_ports i_USER_SMA_CLOCK_N]

#create_generated_clock -name o_SC_CLK_P -source [get_pins Monitoring/clkOut/C] -divide_by 1 [get_ports o_SC_CLK_P] #magic1

#magic2
set_false_path -from [get_ports {{i_GPIO_SW_N} {i_GPIO_SW_S}}] -to [get_clocks -regexp .*] 
#set_false_path -from [get_clocks -regexp .*] -to [get_ports {{o_SC_LOAD} {o_SC_TOMALTA_P} {o_SC_TOMALTA_N}}] 