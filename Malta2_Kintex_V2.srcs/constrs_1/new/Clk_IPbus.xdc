set_property PACKAGE_PIN AD11 [get_ports SYSCLK_N]
set_property PACKAGE_PIN AD12 [get_ports SYSCLK_P]
set_property IOSTANDARD DIFF_SSTL15 [get_ports SYSCLK_P]
set_property IOSTANDARD DIFF_SSTL15 [get_ports SYSCLK_N]

#create_clock -period 1000.000 -name i_EXT_CLK_P -waveform {0.000 500.000} [get_ports i_EXT_CLK_P]
set_property VCCAUX_IO DONTCARE [get_ports SYSCLK_P]

# sma clock
###set_property PACKAGE_PIN L25 [get_ports i_USER_SMA_CLOCK_P]
###set_property PACKAGE_PIN K25 [get_ports i_USER_SMA_CLOCK_N]
###set_property IOSTANDARD LVDS_25 [get_ports i_USER_SMA_CLOCK_N]
###set_property IOSTANDARD LVDS_25 [get_ports i_USER_SMA_CLOCK_P]


#######################################################################################################################################################
#######################################################################################################################################################

#Ethernet PHY pin K3
set_property PACKAGE_PIN L20 [get_ports PHY_RESET]
set_property IOSTANDARD LVCMOS18 [get_ports PHY_RESET]
set_property PACKAGE_PIN H5 [get_ports SGMII_RX_N]
set_property PACKAGE_PIN H6 [get_ports SGMII_RX_P]
set_property PACKAGE_PIN J4 [get_ports SGMII_TX_P]
set_property PACKAGE_PIN J3 [get_ports SGMII_TX_N]
set_property PACKAGE_PIN G8 [get_ports SGMIICLK_Q0_P]
set_property PACKAGE_PIN G7 [get_ports SGMIICLK_Q0_N]

